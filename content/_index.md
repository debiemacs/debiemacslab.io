## Un Debianita en la Corte del Rey Emacs

Este "cuaderno de apuntes" nace con el único fín de crear un órden ante mi desorden, una guía básica a la que acudir, en definitiva un simple cuaderno de anotaciones personal al que pedir auxilio cuando hacemos demasiado uso de nuestro Swap. 

Aquí encontrarás notas tecnológicas principalmente relacionadas con el Software libre, GNU, programación, y todo lo relacionado con el universo Debian/Emacs.


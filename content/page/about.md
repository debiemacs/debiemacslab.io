---
title: Sobre el autor
subtitle: Sobre mí
comments: false
---

Apasionado del software libre y todo el universo que le rodea. 

Ferviente seguidor de Debian, Emacs, I3wm, Gnome, UbPorts, FSF, LibrePlanet, C++... 

Me podéis encontrar dando guerra en:

Diaspora: lfortanet@diasp.eu Twitter: @lfortanet Matrix: @lfortanet:matrix.org Telegram: UBports[ESPAÑOL]


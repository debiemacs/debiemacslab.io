+++
title = "C++ Referencias"
date = 2020-03-27T16:14:00+01:00
lastmod = 2022-10-29T10:15:38+02:00
tags = ["Cpp"]
draft = false
+++

{{< figure src="/ox-hugo/cmasmas.png" >}}

Podríamos decir que existen dos maneras de pasar argumentos a una función, el paso
por valor y el paso por referencia.
Cuando pasamos un argumento por valor estamos pasando una copia del mismo, los
cambios que hagamos en la copia no afectaran al original.

En el paso por referencia, ésta la usamos como un alias, la inicializaremos con
el nombre de otro objeto y debemos tener en cuenta que cualquier cambio en la
Referencia (alias) se hace realmente en el original.

Tienen que ser inicializadas cuando son declaradas y debemos tener en cuenta que
cualquier objeto puede ser referenciado.

**Su intaxis básica es la siguiente:**

```text
---------------------------------------------------------------------------------
Tipo del objeto ---> "&" ----> nombre de la referencia---> "=" ---> nombre target
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
int & rUnaReferencia = unEntero
---------------------------------------------------------------------------------
```

```c
#include <iostream>

int main(){
  using namespace std;
  int intUno;
  //todo lo que le ocurra a rLaRef le va a suceder a intUno
  int &rLaRef = intUno;

  intUno = 5;

  cout << "intUno "<< intUno<<endl;
  cout << "&rLaRef "<< rLaRef<<endl;
}
//Cout nos dara como salida 5 y 5
```

**&amp;** sirve tanto para saber la direccion de una variable (operador Adress-off)
como para declarar una
referencia.

**Pongamos un ejemplo:**

Ambos se refieren al objeto de la misma clase

Escritor Vargas_Llosa;
Escrito &amp;NarradorPeruano = Vargas_Llosa;

**Referencias a Objetos**

Las referencias a Objetos son usados como los objetos mismos.
Para acceder a datos y metodos se hace con el operador de acceso .
rCaballo.ObtenerPeso()

```c
#include <iostream>
class SimpleCaballo
{
public:
  //constructor
  SimpleCaballo (int edad, int peso);
  ~SimpleCaballo(){}
  int ObtenerEdad() {return suEdad;}
  int ObtenerPeso(){ return suPeso;}

private:
  int suEdad;
  int suPeso;

};

SimpleCaballo::SimpleCaballo(int edad, int peso)
{
  suEdad = edad;
  suPeso = peso;

}
int main(){
  using namespace std;
  SimpleCaballo Babieca(5,80);
  SimpleCaballo &rCaballo = Babieca;

  cout << "Babieca tiene: ";
  cout << Babieca.ObtenerPeso() << " kilos"<< endl;
}
```

**Organizar en Funciones**

**Argumentos pasados por Valor.**

```c
//Ejemplo ----Paso por Valor
#include <iostream>
using namespace std;
void intercambiar(int x, int y);

int main(){
  int x = 5;
  int y = 10;

  cout << "Main. Antes intecarmbiar, x: "<< x <<" y: "
       << y << endl;

  intercambiar(x,y);

  //Aqui no se intercambian los valores originales ya que
  //lo que se ha mandado es una copia.
  cout << " Main. Despues intercambiar, x: "<< x <<
    " y: "<< y<< endl;

  return 0;
}

void intercambiar(int x, int y)
{
  int temp;
  cout << "Intercambiar. Antes intercambiar, x: "<<x
       << " y: " << y << endl;

  temp = x;
  x = y;
  y= temp;

cout << "Intercambiar. Despues intercambiar, x: "<<x
       << " y: " << y << endl;


}
```

**Argumentos pasados por Referencia con Punteros**

```c
/Argumentos pasados por Referencia con Punteros./
//Ejemplo ----Paso por referencia usando punteros
//
#include <iostream>
using namespace std;
void intercambiar(int *x, int *y);

int main(){

  int x = 5, y = 10;
  cout << "Main, Antes intercambiar, x: "<< x << " y: "<<
    y << endl;

  intercambiar(&x,&y);

  cout << "Main, Despues intercambiar, x: " << x <<
    " y: " << endl;

  return 0;
}

void intercambiar(int *px, int *py)
{
  int tempo;
  cout << " Intercambiar. Antes intercambiar, *px: "<< *px
       << " *py: " << *py << endl;

  temp = *px;
  *px= *py;
  *py= temp;

  cout << " Intercambiar. Despues intercambiar, *px: "<< *px
       << " *py: " << *py << endl;
}
```

**Argumentos pasados por referencia con Referencias.**

```c

#include <iostream>
using namespace std;
void intercambiar(int &x, int &y);

int main(){

  int x = 5, y = 10;
  cout << "Main, Antes intercambiar, x: "<< x << " y: "<<
    y << endl;
///solamente tenemos que pasarle las variables
  intercambiar(x,y);
//cout nos imprimira los valores cambiados ya que modificamos sus referencias.
  cout << "Main, Despues intercambiar, x: " << x <<
    " y: " << endl;

  return 0;
}
//indicamos que son las referencias, todo lo que ocurre a x o y le ocurre
//a rx y ry,
void intercambiar(int &rx, int &ry)
{
  int tempo;
  cout << " Intercambiar. Antes intercambiar, *px: "<< rx
       << " *py: " << ry << endl;

  temp = rx;
  rx= ry;
  ry= temp;

  cout << " Intercambiar. Despues intercambiar, *px: "<< rx
       << " *py: " << ry << endl;
```

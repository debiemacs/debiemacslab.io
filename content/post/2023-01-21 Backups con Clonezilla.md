+++
title = "Backups con Clonezilla"
date = 2023-01-21T08:46:00+01:00
lastmod = 2023-01-21T08:48:16+01:00
tags = ["linux"]
draft = false
+++

De las multiples maneras que siempre he tenido en cuenta para realizar imágenes de mis
ordenadores, siempre me he quedado con Clonezilla.

Clonezilla es de esas herramientas que sabes que nada va a cambiar radicalmente, siempre va a
estar ahí, una herramienta sólida y accesible para todos.

El único incoveniente, si podemos llamarlo así, es que al ser una herramienta con un uso
esporádico, siempre que voy a hacer uso de ella no recuerdo qué parámetros seguir para hacer
todo correctamente y no crear ningún desaguisado.

Este post está para eso, para cuando el Luis de dentro de unos meses no recuerde ciertas cosas
al 100%, gane en confianza y en tranquilidad al ejecutar estas importantes tareas.


## Clonación del Disco {#clonación-del-disco}

Después de inicializar Clonezilla con los parámetros normales de teclado e idioma, nos encontraremos con la pantalla:

</home/luis/Dropbox/ARCHIVOS_ORG/BLOG/clonezilla1.webp>

Elegiremos **device-device**.

</home/luis/Dropbox/ARCHIVOS_ORG/BLOG/clonezilla2.webp>

Elegimos modo experto, con ello podremos configurar diferentes parámetros haciendo el proceso más eficiente.

</home/luis/Dropbox/ARCHIVOS_ORG/BLOG/clonezilla3.webp>

Nos preguntará qué tipo de clonado queremos, elegiremos **disk to local disk**.

</home/luis/Dropbox/ARCHIVOS_ORG/BLOG/clonezilla4.webp>

Aquí elegiremos nuestro disco de origen.

</home/luis/Dropbox/ARCHIVOS_ORG/BLOG/clonezilla5.webp>

Y aquí elegiremos nuestro disco de destino

Los demás parámetros los dejamos por defecto.


## Restaurar una imagen grabada {#restaurar-una-imagen-grabada}

Iniciamos clonezilla con los parámetros normales.
Como la imagen que tenemos que recuperar se encuentra en una unidad externa, en las opciones de origen
selecionaremos **local dev**.

{{< figure src="/ox-hugo/clonezilla6.jpg" >}}

Elegimos el disco externo donde tengamos la imagen y navegamos hasta donde tengamos la imagen.

{{< figure src="/home/luis/Dropbox/ARCHIVOS_ORG/BLOG/clonezilla7.jpg" >}}

Elegimos modo beginner y seleccionamos **restoredisk**.

{{< figure src="/ox-hugo/clonezilla8.jpg" >}}

Buscamos la imagen y seleccionamos después el origen destino, donque queremos restaurar la imagen.

{{< figure src="/ox-hugo/clonezilla9.jpg" >}}

Aceptamos por defecto lo que venga y comenzará el proceso de restauración.

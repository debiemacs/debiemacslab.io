+++
title = "Lector RSS Noticias en Emacs"
date = 2022-02-12
lastmod = 2022-11-13T08:03:50+01:00
tags = ["emacs"]
draft = false
+++

Tener un lector de las noticias que se producen en los blogs que sigo es una de las experiencias más gratificante
que tengo al usar Emacs, tan sencillo como ir pasando los encabezados de las noticias y la que me interesa pulsar
intro y disfrutar del contenido. Ahí va la configuración!

cd ~/.emacs.d/plugins/

git clone <https://github.com/skeeto/elfeed.git>

En nuestro fichero de configuración añadimos esto:

```text
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PACKAGE: elfeed               ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; source: https://github.com/skeeto/elfeed.git
(add-to-list 'load-path "~/.emacs.d/plugins/elfeed/")
(require 'elfeed)

;; aqui van los feeds que nos interesen
(setq elfeed-feeds
      '(
        "http://blog.chuidiang.com/feed/"
        "http://blog.desdelinux.net/feed/"
        "http://blogubuntu.com/feed"
        "http://cucarachasracing.blogspot.com/feeds/posts/default?alt=rss"
        "http://elarmarioinformatico.blogspot.com/feeds/posts/default"
        "http://es.xkcd.com/rss/"
        "http://feeds.feedburner.com/diegocg?format=xml"
        "http://feeds.feedburner.com/Esbuntucom?format=xml"
        "http://feeds.feedburner.com/GabuntuBlog"
        "http://feeds.feedburner.com/ramonramon"
        "http://feeds.feedburner.com/teknoplof/muQI?format=xml"
        "https://xkcd.com/rss.xml"
        ))
(global-set-key [f7] 'elfeed)
(global-set-key [f8] 'elfeed-update)
```

Con b lo abrimos en nuestro navegador.
Con r marcamos la entrada como leída.

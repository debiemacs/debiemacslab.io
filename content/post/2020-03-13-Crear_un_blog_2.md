+++
title = "Un blog con Hugo y Gitlab - Parte 2"
date = 2020-03-13
lastmod = 2022-11-13T08:03:49+01:00
tags = ["emacs", "hugo", "orgmode"]
draft = false
+++

En esta segunda parte mostraré cómo tengo configurado Emacs con OX-Hugo para
crear posts en el blog que tenemos creado con Hugo.

La idea principal consiste en crear un archivo .ORG único donde iremos alojando
los diferentes posts que vayamos creando, seguidamente con la ayuda de OX-Hugo
generaremos de una manera automática los archivos Mark Down alojándolos en la
carpeta "Post" de nuestro proyecto Gitlab.

Una vez generado el nuevo post realizaremos un commit mediante Git, con ello
publicando el nuevo post.


## Configurando Emacs {#configurando-emacs}

Instalamos Ox-hugo

```text

M-x package-install RET ox-hugo RET
```

En nuestro init.el o .emacs añadiremos las siguientes líneas:

```text

(with-eval-after-load 'ox__
  (require 'ox-hugo))
```

Utilizaremos un único archivo .org para generar los post, con una configuración
general y una configuración básica que aplicaremos a cada post.

Como ejemplo mi archivo .org:

{{< figure src="/ox-hugo/unblogconhugoparte2.png" >}}


## Exportando el artículo a Mark Down {#exportando-el-artículo-a-mark-down}

Para ello usaremos la herramienta de Org-Mode ejecutando C-c - C-e, con H-H
exportaremos el post donde estemos situados, con H-A exportaríamos todos nuestros
posts.

{{< figure src="/ox-hugo/unblogconhugoparte2imagen2.png" >}}


## Realizamos un commit a Gitlab {#realizamos-un-commit-a-gitlab}

Para ello podríamos usar alguna herramienta desde el propio Emacs, yo de momento
uso directamente la terminal desde Emacs, nos posicionamos en nuestro directorio
git y ejecutamos:

```text

git add .
git commit -m "post1"
git push
```

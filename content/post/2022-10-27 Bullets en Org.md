+++
title = "Haciendo más bonito Org-mode"
date = 2022-11-27
lastmod = 2022-11-13T08:03:51+01:00
tags = ["emacs", "orgmode"]
draft = true
+++

Hay diferentes opciones estéticas que permiten darle un aspecto más atractivo a nuestro
ecosistema org. Desde configurar con un color específico cada palabra clave a tener unos
bullets más bonitos que los simples arteriscos.

Ahí va la configuración de varias cosas.

```nil
;;COLORES PALABRAS CLAVE
(setq org-todo-keyword-faces
'(
("TODO"   . (:foreground:"gray0":background "red" :weight bold))
("INICIADA"  . (:foreground:"gray0" :background "blue" :weight bold))
("DONE"   . (:foreground "black":background "green" :weight bold))
("SIGUIENTE"   . (:foreground "gray0" :background "yellow" :weight bold))
("PUBLICADA"   . (:foreground "black":background "orange" :weight bold))
("CANCELADA"  . (:foreground "gray0":background "grey70" :weight bold))
))


;;iconos org mode
(require 'org-superstar)
(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))
```

+++
title = "Calendario en Emacs"
date = 2022-11-05T15:28:00+01:00
lastmod = 2022-11-13T08:10:03+01:00
tags = ["emacs"]
draft = false
+++

Una de las maravillas de organizarte con Emacs, es la **Agenda**, es incleible cómo puedes
controlar cualquier aspecto de tu flujo de trabajo diario con ella, sencilla y clara a la
hora de mostrarte tu día a día.

Con calfw, vamos a poder ver una vista de calendario completa, en la cual se reflejen tanto
nuestras citas, obligaciones como festividades o cualquier fecha relevante que creamo
oportuna.

Lo llamamos con **F5** y la vista quedaría así, reflejando todas nuestras citas y eventos progamados:

{{< figure src="/ox-hugo/calfw.png" >}}

El código para Emacs sería el siguiente:

```nil
;;---------------------------------------------------------------------------
;;----------------------CALENDARIO------------------------------------------
;;--------------------------------------------------------------------------
(use-package calfw
 :ensure t
 :bind ([f5] . mi-calendario) ;; lo llamamos con F5
 :custom
 (cfw:org-overwrite-default-keybinding m)) ;; atajos de teclado de la agenda org-mode
; (setq cfw:display-calendar-holidays nil) ;; esconder fiestas calendario emacs

 (use-package calfw-org
 :ensure t)

;; calendarios a mostrar
(defun mi-calendario ()
  (interactive)
       (cfw:open-org-calendar))
       ; :contents-sources
       ; (list
        ; (cfw:org-create-source))))

;;first day week
(setq calendar-week-start-day 1) ; 0:Domingo, 1:Lunes
(setq calendar-holidays '((holiday-fixed 1 1 "Año Nuevo")
            (holiday-fixed 1 6 "Reyes Magos")
            (holiday-fixed 4 18 "Jueves Santo")
            (holiday-fixed 4 19 "Viernes Santo")
            (holiday-fixed 5 1 "Dia del Trabajador")
            (holiday-fixed 3 5 "Cinco Marzada")
            (holiday-fixed 1 29 "San Valero")
            (holiday-fixed 3 23 "San Chorche")
            (holiday-fixed 10 12 "Día del Pilar")
            (holiday-fixed 11 01 "Todos los Santos")
            (holiday-fixed 11 09 "Almudena")
            (holiday-fixed 12 06 "Constitución")
            (holiday-fixed 12 08 "Inmaculada")
            (holiday-fixed 12 25 "Navidad")
            ))
(setq calendar-week-start-day 1) ;; la semana empieza el lunes
(setq european-calendar-style t) ;; estilo europeo

;;-------------------------------------------------------------------------------------
;;------------------------------------------------------------------------------------
```

+++
title = "Un maravilloso mundo con Org-Mode"
date = 2020-03-13T19:10:00+01:00
lastmod = 2020-03-17T16:58:32+01:00
tags = ["orgmode", "emacs"]
draft = false
+++

Qué voy a decir de Org-Mode!!

{{<figure src="/ox-hugo/OXHUGO.png">}}

Mil posibilidades para poder organizarte, tener todo ordenado, ser más
productivo....Org-Mode te brinda múltiples herramientas para organizar
tu día a día....y todo dentro de Emacs!!!

Dejo mi **"chuleta"** particular de las cosas que uso.


## ATAJOS SENCILLOS {#atajos-sencillos}

```text
TAB sobre un encabezado, oculta o muestra el contenido

Shift - TAB muestra todo el conjunto

C-c C-t Atajo TODO/DONE

Shift-TAB En cualquier posicion, oculta o muestra el contenido de todos los
encabezados.

C-c C-n ir al siguiente encabezado

C-c C-p ir al encabezado anterior

C-c C-f ir al siguiente encabezado del mismo nivel

C-c C-b ir al encabezado anterior del mismo nivel

C-c C-u ir al encabezado padre

M-FlechaDerecha agregar un "*" al encabezado

M-FlechaIzquierda quitar un "*" al encabezado

M-FlechaArriba Si el encabezado superior es del mismo nivel, intercambia el
lugar de los encabezados, es decir mueve el actual hacia arriba.
```


## RESALTADO FUENTES {#resaltado-fuentes}

```text
*bold* ==> bold
~code~ ==> code
+strike+ ==> strike
/italic/ ==> italic
=verbatim= ==> code
_underline_ ==> underline
```


## IMÁGENES INLINE {#imágenes-inline}

En el encabezado inicializamos siempre el documento con:  #+STARTUP:inlineimages

Incluimos la ruta en la zona del documento que queramos con dobles corchetes
tanto al comienzo como al final.

```text
[[file:rutax\rutaxx\fichero.png]]
```

Pulsando la combinación C-c C-x C-v mostraremos o ocultaremos todas las imágenes
del documento.


## ENLACES {#enlaces}

En principio usamos links sencillos, con añadirlo directamente es suficiente, nos
lo implementará automaticamente.

```text
https:\\debian.es
```


### Enlaces soportados {#enlaces-soportados}

```text
http://www.astro.uva.nl/~dominik          en la red
doi:10.1000/182                           DOI para un recurso electronico
file:/home/dominik/images/jupiter.jpg     fichero, ruta absoluta
/home/dominik/images/jupiter.jpg          igual que arriba
file:papers/last.pdf                      fichero, ruta relativa
./papers/last.pdf                         igual que arriba
file:/myself@some.where:papers/last.pdf   fichero, ruta a una maquina remota
/myself@some.where:papers/last.pdf        igual que arriba
file:sometextfile::NNN                    fichero, saltar al numero de linea
file:projects.org                         otro fichero Org
file:projects.org::some words             buscar texto en fichero Org[fn:35]
file:projects.org::*task title            buscar encabezado en fichero Org[fn:36]
file+sys:/path/to/file                    abrir via OS, como doble-click
file+emacs:/path/to/file                  fuerza apertura con Emacs
docview:papers/last.pdf::NNN              abrir en modo doc-view en la pagina
id:B7423F4D-2E8A-471B-8810-C40F074717E9   Enlazar con ecabezado por ID
news:comp.emacs                           enlace Usenet
mailto:adent@galaxy.net                   enlace Mail
mhe:folder                                enlace a carpeta MH-E
mhe:folder#id                             enlace a mensage MH-E
rmail:folder                              enlace a carpeta RMAIL
rmail:folder#id                           enlace a mensaje RMAIL
gnus:group                                enlace a grupo Gnus
gnus:group#id                             enlace a articulo Gnus
bbdb:R.*Stallman                          enlace BBDB (con regexp)
irc:/irc.com/#emacs/bob                   enlace IRC
info:org#External links                   enlace a nodo o indice Info
shell:ls *.org                            Un comando shell
elisp:org-agenda                          Comando Interactivo Elisp
elisp:(find-file-other-frame "Elisp.org") formate de evaluacion Elisp
```


## CHECKBOXES {#checkboxes}


### <span class="todo TODO_">TODO </span> Ejemplo Checkboxes {#ejemplo-checkboxes}

-   <code>[50%]</code> ESTUDIAR
    -   [X] Coger El LIBRO
    -   [ ] PRIMERA LECCION
-   [X] HACERLOS

Con Ctrol C C chequearemos una casilla.
Si al crearla añadimos <code>[%]</code> o <code>[/]</code> nos mostrará tanto por ciento realizado.


## BLOQUES {#bloques}

Podemos introducir bloques y nos lo adaptará al formato que queramos.
Diferentes tipos:

```text
EXAMPLE ejemplos
HTML seccion de codigo a exportar tal cual
LaTex seccion de codigo a exportar tal cual
SRC bloque de codigo
QUOTE bloque de cita
```

En el caso de SRC añadimos el lenguaje que queremos que refleje: c, java, php...

```java

package com.gm.mundopc;

public class Raton extends DispositivoEntrada {

    private final int idRaton;
    private static int contadorRatones;

    //constructor que inicializa las variables
    public Raton(String tipoEntrada, String marca) {
        super(tipoEntrada, marca);
        idRaton = ++contadorRatones;
    }

    @Override
    public String toString() {
        return "Raton{" + "idRaton=" + idRaton + ", " + super.toString();
     }
}
```


## EXPORTAR {#exportar}

Exportamos con la combinación C-c , C-e , pero antes añadimos en el enbabezado
del documento:

Para colocar imagenes dentro del contenido que exportes simplemente coloca la

direccion de la imagen, asi:

```text
[[[[file:mi-imagen.png]]]]
```

```text
#+OPTIONS: toc:nil num:nil h:7 html-preamble:nil html-postamble:nil html-scripts:nil html-style:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="worg-data/worg.css" />
```


## LISTA TODO GLOBAL {#lista-todo-global}

Una de las más interesantes que suelo usar, podemos combinar diferentes archivos org
para gestionar de un vistazo las tareas pendientes:

Añadimos en nuestro archivo de configuración Emacs:

```text
(setq org-agenda-files (list "~/org/trabajo.org"
                             "~/org/clase.org"
                             "~/org/casa.org"))
```

Al presionar C-c a t , org-mode escaneará todas las tareas pendients en los
archivos que hayamos configurado en list y nos lo mostrará.

Pulsando t , hacemos un DONE. Con INTRO vamos directamente a esa tarea.


## PLANIFICANDO TAREAS CON LA AGENDA {#planificando-tareas-con-la-agenda}

Añadir a nuestro archivo emacs de configuración esto:

```text
;; Las siguiente líneas son siempre necesarias. Elige
;; tus propias claves
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)
```

C-c C-s aparece org-Schedule, y vamos el calendario, elegimos fecha.
Org-mode inserta una marca después del TODO.

C-c a a para org-agenda. Se mostrará una vista con los items planificados.

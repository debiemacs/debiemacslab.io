+++
title = "Notas EMACS"
date = 2019-10-12
lastmod = 2022-11-13T08:03:48+01:00
tags = ["emacs"]
draft = false
+++

En este post voy reflejando diferentes configuraciones que hago uso en Emacs,
aquí encontraremos un pequeño cajón desastre.


## SELECCIONAR/COPIAR/PEGAR {#seleccionar-copiar-pegar}

```text
//Ctrl + Espace // Ctrl + w // Ctrl + y//
```


## CHULETA EMACS {#chuleta-emacs}

```text
Mover por páginas

Ctrl v – (siguiente)
Alt v – (anterior)
Ctrl l – Limpiar la pantalla y volver a mostrar todo el texto, moviendo el texto alrededor del cursor al centro de la pantalla.
Moverse entre el texto

Ctrl f – Moverse un caracter hacia adelante
Ctrl b – Moverse un caracter hacia atrás
Alt f – Moverse una palabra hacia adelante
Alt b – Moverse una palabra hacia atrás

Ctrl n – Moverse a la siguiente línea
Ctrl p – Moverse a la línea anterior
Ctrl a – Moverse al comienzo de una línea
Ctrl e – Moverse al final de una línea
Alt a – Moverse al inicio de una oración
Alt e – Moverse al final de una oración
Alterando el texto

Ctrl k – kill, «mata» el texto. Esto implica que lo borra, pero lo mantiene guardado y puede ser recuperado con Ctrl Y.
Ctrl d – Borra un caracter (borra, no mata, no puede ser recuperado).
Alt d – Borra palabras

Ctrl @ / Ctrl Espacio – marca el texto (primera marca)
Ctrl w – corto texto desde la marca de texto.
Alt w – copia texto desde la marca de texto.
Ctrl h – Marcar todo el buffer, como «Seleccionar todo».

Ctrl y – «Yanks text», pega el texto matado o cortado/copiado con w.
Alt y – Recorre yanks previos, podemos recuperar algo que matamos varios Ctrl k antes.

Ctrl g – Cancelar comandos
Ctrl x u – Deshacer
Ctrl / – Deshacer
Ctrl x – Comandos

Varios comandos se realizan presionando Ctrl x y después una tecla, o Ctrl una tecla. Por ejemplo el «deshacer» de las líneas anteriores, se tiene que presionar Ctrl x (soltar) y presionar u.

Ctrl x Ctrl f – «Visitar» nuevo archivo (si existe lo abre, sino lo crea).
Ctrl g – Cancelar comandos

Buffers

Ctrl x Ctrl b – Listar buffers. En Emacs no hay «pestañas» o «ventanas». Cada archivo se abre en un buffer. Con este comando vemos todos los buffers que hayan abiertos en esta sesión de Emacs.
Ctrl x b – Cambiar de buffer. Muy práctico, podemos usar el tabulador para autocompletar el nombre de un buffer.
Ctrl x k – Cerrar buffer
Ctrl x Ctrl s – Guardar un archivo
Ctrl x s – Guardar buffers

Ctrl X Ctrl C – Salir de Emacs
Ctrl z – Suspende la sesion de emacs. Volvemos desde la consola con fg

Alt x – Modo comandos. Nos permite ejecutar comandos como los siguientes:
Alt x replace-string – reemplazo de strings
Alt x recover file – levantar respaldo del archivo

Ctrl S – Buscar strings incrementalmente (con Ctrl S sigo buscando hacia adelante, con Ctrl R busco hacia atrás y con enter termino la busqueda)
Alt % buscar ENTER reemplazar – Buscar y reemplazar.

Ctrl x 2 – Divide la ventana en 2 de forma horizontal.
Ctrl x 3 – Divide la ventana en 2 de manera vertical.
Ctrl x 1 – Deja solo una ventana abierta.
Ctrl Alt v – scrollea la ventana donde no tengo el foco.
Ctrl x o – cambia el cursor de una ventana a otra.
```

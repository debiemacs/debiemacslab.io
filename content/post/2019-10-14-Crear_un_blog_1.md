+++
title = "Un blog con Hugo y Gitlab - Parte 1"
date = 2019-10-14
lastmod = 2022-11-13T08:03:48+01:00
tags = ["orgmode", "git", "emacs", "hugo"]
draft = false
+++

Cuando me planteé crear un blog personal de anotaciones y de apuntes,
tenía varias cosas muy claras, quería un blog sin publicidad, que fuera sencillo
y que toda la carga de trabajo la pudiera realizar desde Emacs.

Al principio lo realicé con ORG-Page, y lo cierto es que funcionaba muy bien,
pero mis escasos conocimientos de CSS, y la manera tan particular que tenía
ORG-Page de gestionar los temas de las plantillas, hicieron que finalmente lo
desechara, cierto es que lo logré configurar muy correctamente, estéticamente
como me gustaba, pero el hecho de no saber realmente cómo trabajaba internamente
me creaba una sensación agridulce en el control del blog.

No quiero decir que ORG-Page sea difícil, o menos válido que cualquier otra
opción, sólo que yo no fuí capaz de sentirme cómodo totalmente. Le podéis echar
un vistazo si queréis, <http://encodigoemacs.gitlab.io> , os dejo también como
referencia el del compañero Notxor creado integramente con ORG-Page, un blog muy
bueno, <https://notxor.nueva-actitud.org>.

Dentro de las diferentes opciones que hay, la razón que me hizo decantarme
por Hugo, fue que aunque trabaja con el contenido en Markdown, tienes la
posibilidad con otras herramientas de poder seguir editando en ORG y Emacs,
también la gran cantidad de plantillas que existían para temas era un aliciente,
ya que no quería perder demasiado el tiempo en ello.

La herramienta que permite ese nexo de unión entre Hugo y Org-Mode es OX-Hugo,
fue un gran descrubimiento que me vino del excelente blog
<http:%5C%5Celblogdelazaro.gitlab.io> , él integra también Hugo con OX-hugoobteniendo
un excelente resultado.

En este ciclo de posts voy a "intentar" explicar cómo realizo éste blog, paso a
paso.

Vayamos por partes, lo primero consistirá en crear nuestro espacio de trabajo en
<https:%5C%5Cgitlab.com> , Gitlab nos permitirá con sus propias herramientas generar
una plantilla principal de Hugo, obteniendo en pocos pasos un blog totalmente
operativo.


## Creamos un Grupo {#creamos-un-grupo}

{{< figure src="\IMAGEN_1_BLOG.png" >}}

Rellenamos todos los datos, seleccionando el Nivel de visibilidad en Público
y seleccionamos CREATE GROUP.

{{< figure src="\IMAGEN_0_1_BLOG.png" >}}


## Creamos el Proyecto {#creamos-el-proyecto}

Ahora ya tenemos creado y seleccionado un Grupo de Trabajo, lo que haremos
ahora será crear un proyecto, para ello seleccionamos dentro de nuestro
grupo recién creado Nuevo Proyecto.

{{< figure src="\IMAGEN_0_BLOG.png" >}}


## Creamos proyecto inicial HUGO {#creamos-proyecto-inicial-hugo}

Seleccionamos crear desde una plantilla y seleccionamos HUGO.

{{< figure src="\IMAGEN_2_BLOG.png" >}}


## Clonamos a Local nuestro nuevo Proyecto {#clonamos-a-local-nuestro-nuevo-proyecto}

GitLab nos habrá generado todo el árbol de archivos y directorios necesarios
del blog que hemos creado, nos habrá creado el blog con una plantilla genérica y
sólo nos quedará clonarlo a nuestro equipo para poder gestionar todas las
modificaciones que hagamos de una manera cómoda.

{{< figure src="\IMAGEN_3_BLOG.png" >}}

Para clonarlo pulsaremos la pestaña Clonar, copiaremos la ruta que nos dé y
ejecutamos en nuestro equipo:

\#+begin_src bash
git clone xxxxxxxxxxxxxxx
\#+end_src code


## Ejecutamos Pipelines {#ejecutamos-pipelines}

Vamos en el menú de la izquierda a la sección CI/CD Pipelines y pulsamos
ejecutar Pipeline.

{{< figure src="\IMAGEN_4_BLOG.png" >}}

En la sección Configuración ---Pages nos aparecerá la ruta del blog creado,
en unos minutos lo tendremos operativo.

{{< figure src="\IMAGEN_5_BLOG.png" >}}

Por último iremos a Configuración----General----Avanzado----Cambiar la Ruta,
introducimos una ruta más directa...nombre_proyecto.gitlab.io

{{< figure src="\IMAGEN_6_BLOG.png" >}}

Esperamos unos minutos a que se genere de nuevo el blog y tendremos ya creado
un blog con una plantilla genérica tanto en la web como en nuestra máquina local.

{{< figure src="\IMAGEN_7_BLOG.png" >}}

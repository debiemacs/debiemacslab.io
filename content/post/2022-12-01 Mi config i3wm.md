+++
title = "Mi archivo Config en I3WM"
date = 2022-11-01T22:44:00+01:00
lastmod = 2022-11-13T08:03:51+01:00
tags = ["i3"]
draft = false
+++

{{< figure src="/ox-hugo/i3wmlogo.png" >}}

Llevo ya muchos años en Linux, y dos entornos de escritorio han predominado en mi vida linuxera,
uno de ellos es Gnome y el otro, el **genial i3wm**.

Éste último siempre he estado dando vueltas cómo mejorarlo, por suerte hay gente muy experta que
está dispuesta a mostrar sus habilidades en las configuraciones.

Mil gracias como siempre a gente tan genial como **Atareao, Notxor, Ugeek, y todos los compañeros de
los canales de Telegram en los grupos de Emacs y Org**.

Yo he ido recabando lo que mejor se adaptaba a mi flujo diario, y que os voy a decir, **no puedo
estar más cómodo**.

Así de bonico luce!!!

{{< figure src="/ox-hugo/miconfigi3wm.png" >}}

Dejo toda mi configuración, intentaré ir explicándola punto por punto.

```nil
# i3wm config file
# Autor:
#       Kraka
# Version:
#       v1.8: 01/11022 configuración i3wm

# Establecemos que la tecla principal para trabajar con i3 sea la de Windows
set $mod Mod4

# Establecemos que la fuente del sistema sea la System San Francisco y que el tamaño sea 10
#font pango:System San Francisco Display 10

# Al tener como mínimo 2 ventanas abiertas presionamos la tecla Windows, Posicionamos el ratón en uno de los bordes de la ventana, presionamos el botón derecho del ratón y lo arrastramos para modificar el tamaños de las ventanas
floating_modifier $mod

# El atajo de teclado para abrir una terminal será windows + Enter. Aquí también podemos seleccionar la terminal que queremos que se abra.
bindsym $mod+Return exec gnome-terminal

# La combinación de teclas para cerrar una ventana activa es windows+shift+q
bindsym $mod+Shift+q kill

# Definimos el tema y los atajos de teclado para el funcionamiento de Rofi. Si cuando Rofi está abierto presionamos Shift+cursores derecha e izquierda obtendremos funcionalidades adicionales.
#set $rofi_theme "/usr/share/rofi/themes/dmenu.rasi"
#bindsym Ctrol+d exec rofi -show run -config $rofi_theme -font "System San Francisco Display 12"
#bindsym Ctrol+Shift+space exec rofi -show window -config $rofi_theme
#bindsym $mod+d exec --no-startup-id i3-dmenu-desktop

# Rofi menu launcher
set $rofi_theme "/usr/share/rofi/themes/Adapta-Nokto.rasi"
bindsym $mod+d exec rofi -show run -config $rofi_theme
bindsym $mod+Shift+d exec rofi -show window -config $rofi_theme
# Combinación de teclas a usar para movernos entre ventanas. En mi caso uso la tecla windows+los cursores. En vez de los cursores también se pueden usar j,k,l y ñ
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+ntilde focus right
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Para mover la ventana seleccionada arriba, abajo izquierda o derecha presionando la tecla Win+Shift+cursores. En vez de los cursores también se pueden usar j,k,l y ñ
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+ntilde move right
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# Para definir que la siguiente ventana que abramos lo haga abajo o al lado de la actual que tenemos activa. Si os fijáis bien veréis un marco de color verde que indicará la posición donde se ubicará la nueva ventana
bindsym $mod+h split h
bindsym $mod+v split v

# Para poner la ventana seleccionada a pantalla completa presionar Win+f
bindsym $mod+f fullscreen toggle

# Para hacer que las ventanas se dispongan/abran en modo tiling, pestaña o apiladas. La opción predeterminada es el modo tiling
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# Para transformar una ventana de anclada a flotante y viceversa
bindsym $mod+Shift+space floating toggle

# Si la ventana activa actual está en modo tiling y queremos que la ventana activa sea una ventana flotante deberemos presionar Win+Espacio
bindsym $mod+space focus mode_toggle

# La tecla Win+a sirve para agrupar ventanas. Una vez agrupadas podremos insertar nuevas ventanas en la posición que exactamente necesitemos.
bindsym $mod+a focus parent

# Workspace naming
set $workspace1 "1: Terminal "
set $workspace2 "2: Navegador "
set $workspace3 "3: Emacs "
set $workspace4 "4: Archivos "
set $workspace5 "5: Mensajería "
set $workspace6 "6"
set $workspace7 "7"
set $workspace8 "8"
set $workspace9 "9"
set $workspace10 "10: Musica "

# switch to workspace
bindsym $mod+1 workspace $workspace1
bindsym $mod+2 workspace $workspace2
bindsym $mod+3 workspace $workspace3
bindsym $mod+4 workspace $workspace4
bindsym $mod+5 workspace $workspace5
bindsym $mod+6 workspace $workspace6
bindsym $mod+7 workspace $workspace7
bindsym $mod+8 workspace $workspace8
bindsym $mod+9 workspace $workspace9
bindsym $mod+0 workspace $workspace10



#exec -no-startup-id i3-msg 'workspace 2:Emacs; exec emacs'

bindsym $mod+Shift+m exec emacs

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $workspace1
bindsym $mod+Shift+2 move container to workspace $workspace2
bindsym $mod+Shift+3 move container to workspace $workspace3
bindsym $mod+Shift+4 move container to workspace $workspace4
bindsym $mod+Shift+5 move container to workspace $workspace5
bindsym $mod+Shift+6 move container to workspace $workspace6
bindsym $mod+Shift+7 move container to workspace $workspace7
bindsym $mod+Shift+8 move container to workspace $workspace8
bindsym $mod+Shift+9 move container to workspace $workspace9
bindsym $mod+Shift+0 move container to workspace $workspace10


#Asignando programas a espacios de trabajo
assign [class = "Gimp"] → $workspace10
assign [class = "Emacs"] → $workspace3
assign [class = "Xpdf"] → $workspace2
assign [class = "Google-chrome"] → $workspace2
assign [class = "gnome-terminal"] → $workspace1
assign [class = "Nautilus"] → $workspace4
assign [class = "Thunderbird"] → $workspace4
assign [class = "TelegramDesktop"] → $workspace5
assign [class = "Thunar"] → $workspace4

bindsym $mod+Shift+x exec xpdf
bindsym $mod+Shift+g exec gimp
bindsym $mod+Shift+n exec firefox
bindsym $mod+Shift+i exec gnome-terminal -x sh -c "irssi"
bindsym $mod+Shift+t exec gnome-terminal -x sh -c "mc"
bindsym $mod+Shift+y exec gnome-terminal -x sh -c "evolution"
bindsym $mod+Shift+u exec gnome-terminal -x sh -c "newsbeuter"
bindsym $mod+Shift+p exec libreoffice --nologo --writer
bindsym $mod+Shift+h exec eclipse

# Atajo de teclado para recargar el archivo de configuración de i3
bindsym $mod+Shift+c reload

# Atajo de teclado para reiniciar por completo el escritorio i3. No te saca de la sesión ni pierdes ningún tipo de información.
bindsym $mod+Shift+r restart

# Atajo de teclado para cerrar completamente la sesión de i3. Una vez cerrada la sesión veremos la ventana de login de Lightdm
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# Configuración para redimensionar las ventanas
mode "resize" {
        # Vim bindings
        bindsym j resize shrink width 3 px or 3 ppt
        bindsym k resize grow height 3 px or 3 ppt
        bindsym l resize shrink height 3 px or 3 ppt
        bindsym ntilde resize grow width 3 px or 3 ppt

        # Arrow bindings
        bindsym Left resize shrink width 3 px or 3 ppt
        bindsym Down resize grow height 3 px or 3 ppt
        bindsym Up resize shrink height 3 px or 3 ppt
        bindsym Right resize grow width 3 px or 3 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

# Combinación de teclas para activar el modo redimensionar ventans
bindsym $mod+r mode "resize"

# Configuración de colores mostrados en pantalla
set $bg-color	         #196609
set $border-focus	 	 #8c8c8c
set $inactive-bg-color   #56BA7C
set $text-color          #00000
set $inactive-text-color #00000
set $urgent-bg-color     #00000
set $blue-color          #5DA8F4

# Colores de las ventanas
#                       Border              Background         text                 indicator
client.focused          $border-focus       $border-focus      $text-color          #00ff00
client.unfocused        $inactive-bg-color  $inactive-bg-color $inactive-text-color #00ff00
client.focused_inactive $inactive-bg-color  $inactive-bg-color $inactive-text-color #00ff00
client.urgent           $blue-color         $blue-color        $text-color          #00ff00


# Se define parte de la configuración de la barra i3blocks
bar {
        # Bar program
#       status_command i3blocks -c ~/.i3/i3blocks.conf
      font xft: FontAwesome 12

#       status_command i3status
        status_command i3blocks -c ~/.config/i3/i3blocks.conf

        # Position of the bar
        position top

        # Bar colors
        colors {
      background $bg-color
          separator #757575
      #                  border             background         text
      focused_workspace  $bg-color          $bg-color          $text-color
      inactive_workspace $inactive-bg-color $inactive-bg-color $inactive-text-color
      urgent_workspace   $blue-color        $blue-color        $text-color
        }
}

# Quita la ventana de título y se define el grosor del borde que recubre las ventanas
for_window [class="^.*"] border pixel 1

# Mutear, incrementar y bajar el volúmen con el teclado "pactl tiene que estar instalado"
#bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +5% #increase sound volume
#bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5% #decrease sound volume
#bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle # mute sound
bindsym Mod1+Up exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +5% #increase sound volume
bindsym Mod1+Down exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -5% #decrease sound volume
bindsym Mod1+m exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle # mute sound

# Win+F9 para ir cambiando la salida de audio
#bindsym $mod+F9  exec ~/.config/i3/scripts/toggle_sink.sh

# Para incrementar o disminuir el brillo del monitor. Hace falta tener instalado xbacklight. Como no uso la opción la descomento
#bindsym XF86MonBrightnessUp exec xbacklight -inc 20 # increase screen brightness
#bindsym XF86MonBrightnessDown exec xbacklight -dec 20 # decrease screen brightness

bindsym XF86MonBrightnessUp exec xbacklight -inc 10 # increase screen brightness
bindsym XF86MonBrightnessDown exec xbacklight -dec 10 # decrease screen brightness

# Touchpad controls
#bindsym XF86TouchpadToggle exec /home/joan/bin/toggletouchpad # toggle touchpad

# Invertir el scroll del trackpad "Solo activar en equipos con trackpad"
# exec_always --no-startup-id synclient NaturalScrolling=1 VertScrollDelta=-113

# Deshabitar el trackpad mientras escribimos "Solo activar en equipos con trackpad"
# exec syndaemon -i 0.3 -t -K -R

# Asignación de las teclas multimedia
#bindsym XF86AudioPlay exec play_pause
#bindsym XF86AudioPause exec play_pause
#bindsym XF86AudioNext exec playerctl next
#bindsym XF86AudioPrev exec playerctl previous

# Atajos de teclado para lanzar programas.
#bindsym $mod+x exec i3lock --color "$bg-color" (i3lock es otra opción para bloquear la pantalla, para usarlo hay que instalar el paquete i3lock)
#bindsym $mod+Shift+g exec google-chrome
#bindsym $mod+Shift+m exec emacs
#bindsym $mod+Shift+b exec blueman-manager

# Seleccionar las aplicaciones que arrancar al iniciar i3
#exec --no-startup-id telegram-desktop
exec --no-startup-id emacs
exec --no-startup-id google-chrome
exec --no-startup-id gnome-terminal
# Para añadir el applet de Nework manager en el panel
exec --no-startup-id nm-applet
exec --no-startup-id dropbox start
#exec --no-startup-id redshift-gtk

# Hacer que se active el teclado númerico cada que que se rearranque el gestor de ventanas i3. Hay que tener instalado el paquete numlockx
exec_always --no-startup-id numlockx on

# Para establecer un fondo escritorio cada vez que se arranque o rearranque i3. "sudo apt install feh"
exec_always feh --bg-scale "/home/luis/Imágenes/Fondos de escritorio/397848.jpg"

# Hacer que la ventana de redacción de un email en thunderbird se abra en modo flotante "para ver las clases de las ventanas podemos usar xprop"
for_window [window_role="Msgcompose" class="(?i)Thunderbird"] floating enable

# Fijamos que la tecla imprimir pantalla sea encargada de realizar capturas de pantalla con la utilidad de XFCE. Una alternatica es usar flameshot
bindsym Print exec --no-startup-id xfce4-screenshooter
#bindsym Control+Print exec --no-startup-id flameshot gui

# Habilitar geoclue
# exec --no-startup-id /usr/lib/geoclue-2.0/demos/agent

# Combinación de teclas para bloquear la pantalla (Requiere el uso de Lightdm)
bindsym $mod+shift+F2 exec dm-tool lock

# Habilitar un compositor para tener transparencias y transiciones suaves. "sudo apt install compton"
exec compton -f

# Para cerrar el ordenador presionar la combinación de teclas Ctrl+Alt+S
bindsym $mod+mod1+s exec  /sbin/poweroff
#bindsym $mod+Shift+e exec i3-msg exit

bindsym --release $mod+Print exec "scrot -s -e 'mv $f ~/Dropbox/ARCHIVOS_ORG/BLOG'"
```

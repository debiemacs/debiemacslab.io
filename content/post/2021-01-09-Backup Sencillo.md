+++
title = "Backup genérico"
date = 2021-01-10
lastmod = 2022-11-13T08:03:50+01:00
tags = ["general"]
draft = false
+++

Un backup sencillo de nuestros archivos, junto a su explicación elemental de funcionamiento.
Comprimir

```text
tar cvfz /media/usb/copia.tgz /usr/luis/Documentos
```

Descomprimir

```text
tar xvfz /media/usb/copia.tgz /usr/xxxx/Documentos
```

Script automático

```text
backup_diario.sh

-----------------------------
#!/bin/bash

_fecha=$(date +"%m_%d_%Y")
_destino="/media/usb/copia("$_fecha").tgz"

tar cfz $_destino /home/luis/Documentos

-----------------------------
#+end_src
Ejecutamos contrab -e y añadimos:
#+begin_src example
PATH=/bin
0 0 * * * /root/backup_diario.sh
:emacs
```

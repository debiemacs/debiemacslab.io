+++
title = "Irc con Irssi"
date = 2020-03-14T16:40:00+01:00
lastmod = 2022-11-13T08:03:49+01:00
tags = ["terminal"]
draft = false
+++

Siguen resistiendo los canales Irc al Whatsapp, Telegram, Matrix.., sobre todo
para temas técnicos y comunicaciones directas vienen muy bien, en Irc muy rara
vez te encontrarás un off topic, se respira máxima seriedad.

Tenemos Erc para Emacs funcionando muy bien pero a mí personalmente me gusta
tener por separado la ejecución de Irc, y para ello no hay nada mejor que Irssi.

Irssi es una aplicación de consola que te va a permitir un control total de los
canales y servidores, fácil y sin distraciones.

Una vez instalado Irssi pasaremos directamente a configurar el config, lo
encontraremos en nuestro home, directorio oculto .irssi.

Configuración Inicio automático:

```text
/network add mired //Creamos una red por si usamos diferentes servidores.

/server add -4 -auto -ircnet mired irc.freenode.net 6667
```

Algunos comandos básicos:

```text
/server list
/channel list
/msg usuario <mensaje> //mensaje privado
/nick <nuevo nick>
/exit //desconectamos del servidor
/whois <usuario>
/channel //nos muestra información del canal
```

Os muestro mi archivo de configuración con las partes más relevantes.

```nil
servers = (
  {
    address = "irc.freenode.net";
    chatnet = "mired";
    port = "6667";
    password = "XXXX";
    use_tls = "no";
    tls_verify = "no";
    autoconnect = "yes";
  }
);

chatnets = {

  Freenode = {
    type = "IRC";
    max_kicks = "1";
    max_msgs = "4";
    max_whois = "1";
  };

  mired = { type = "IRC"; };
};

//Configuraremos los canales que queremos que se autoejecuten

channels = (
  { name = "#emacs"; chatnet = "mired"; autojoin = "Yes"; },
  { name = "#emacs-es"; chatnet = "mired"; autojoin = "Yes"; },
  { name = "#debian"; chatnet = "mired"; autojoin = "Yes"; },
  { name = "#debian-es"; chatnet = "mired"; autojoin = "Yes"; },
  { name = "#libreplanet"; chatnet = "mired"; autojoin = "Yes"; },
  { name = "#ircsource"; chatnet = "IRCSource"; autojoin = "No"; },
  { name = "#netfuze"; chatnet = "NetFuze"; autojoin = "No"; },
  { name = "#oftc"; chatnet = "OFTC"; autojoin = "No"; },
  { name = "silc"; chatnet = "SILC"; autojoin = "No"; }
);


};

//Configuramos información básica, nuestro Nick y el mensaje de despedida.
settings = {
  core = {
    real_name = "luis";
    user_name = "luis";
    nick = "Mi_nick";
    quit_message = "I'll be back soon guys";
  };
  "fe-text" = { actlist_sort = "refnum"; };
};
```

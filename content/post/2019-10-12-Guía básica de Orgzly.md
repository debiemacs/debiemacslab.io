+++
title = "Guía básica de Orgzly"
date = 2020-03-27T16:14:00+01:00
lastmod = 2022-11-13T08:03:48+01:00
tags = ["orgmode", "orgzly"]
draft = false
+++

Orgzly es una aplicación disponible para Android, mediante Fdroid. Nos va a
permitir crear, editar nuestras notas Org desde nuestro móvil, yo la
utilizo junto a otra gran apliación Synthing, con esta logro sincronizar en
diferentes dispositivos todo mi mundo Org.

Dejo la explicación de funcionamiento oficial, me quedará pendiente editar
y realizar una explicación más clara y particular de todas sus funcionalidades.

---

¡Bienvenido a Orgzly!


## Notas {#notas}


### Haga clic en la nota para seleccionarla {#haga-clic-en-la-nota-para-seleccionarla}

Se pueden seleccionar múltiples notas.


### Plegar una nota que contiene subnotas haciendo clic en el icono a su lado {#plegar-una-nota-que-contiene-subnotas-haciendo-clic-en-el-icono-a-su-lado}


#### No hay límite en el número de niveles que se pueden tener {#no-hay-límite-en-el-número-de-niveles-que-se-pueden-tener}

<!--list-separator-->

-  Utilice esto para organizar mejor sus notas


### Una nota puede tener etiquetas                               " <span class="tag"><span class="tag1">tag1</span><span class="tag2">tag2</span></span> {#una-nota-puede-tener-etiquetas}


#### Las subnotas heredan las etiquetas <span class="tag"><span class="tag3">tag3</span></span> {#las-subnotas-heredan-las-etiquetas}

Si usted busca notas con la etiqueta "tag1", esta nota también se devolverá.
"


### Una nota puede tener un estado {#una-nota-puede-tener-un-estado}

Puede configurar cualquier número de estados a utilizar: TODO, NEXT, DONE, etc.


#### Hay dos _tipos_ de estados: to-do y done {#hay-dos-tipos-de-estados-to-do-y-done}


#### <span class="org-todo done DONE">DONE</span> Esta es una nota con el tipo de estado _done_ {#esta-es-una-nota-con-el-tipo-de-estado-done}


### Una nota puede tener un momento de inicio programado {#una-nota-puede-tener-un-momento-de-inicio-programado}


#### La fecha puede ser una repetición {#la-fecha-puede-ser-una-repetición}


### También se puede establecer un tiempo límite {#también-se-puede-establecer-un-tiempo-límite}


### Se soportan los recordatorios para horas programadas y de tiempo límite {#se-soportan-los-recordatorios-para-horas-programadas-y-de-tiempo-límite}

Estan inhabilitadas por defecto y necesitan estar habilitadas en Configuración.


### Una nota puede tener una prioridad {#una-nota-puede-tener-una-prioridad}

Puedes cambiar el número de prioridades en Configuración. También puedes cambiar la prioridad por defecto - la prioridad asumida para notas sin una puesta.


### Una nota puede incluir enlaces {#una-nota-puede-incluir-enlaces}

Marcar un número de teléfono (tel:555-0199), enviar SMS (sms:555-0199), componer un correo electrónico (<mailto:support@orgzly.com>) o visitar una página web ([Orgzly.com](http://www.orgzly.com)).

También puede enlazar a otra nota o bloc de notas dentro de la aplicación.

Consulte <http://www.orgzly.com/help#links> para obtener más información.


### Están soportados formatos tipográficos básicos {#están-soportados-formatos-tipográficos-básicos}

Puede escribir palabras en **negrita**, _italica_, <span class="underline">subrayadas</span>, = cita literal=, `código` y ~~tachadas~~.


### Se pueden crear listas de verificación {#se-pueden-crear-listas-de-verificación}

-   [X] Tarea 1
-   [ ] Tarea 2
-   [ ] Tarea 3

Pulsa la caja de verificación para alternar su estado. Pulsa el botón de nueva línea al final de la línea para crear un nuevo eleme


## Buscar {#buscar}


### Existen muchos operadores de búsqueda soportados {#existen-muchos-operadores-de-búsqueda-soportados}

Puede buscar notas por estado, etiqueta, fecha de inicio, fecha límite, etc.

Visite <http://www.orgzly.com/help#search> para aprender más.


### Las consultas se pueden guardar como accesos rápidos {#las-consultas-se-pueden-guardar-como-accesos-rápidos}

Pruebe los ejemplos de búsqueda del cajón de navegación y observe las consultas
que utilizan.

Puede crear sus propias búsquedas y guardarlas haciendo clic en "Búsquedas" en
el cajón de navegación.


## Sincronizando {#sincronizando}


### Los cuadernos pueden guardarse como archivos de texto plano {#los-cuadernos-pueden-guardarse-como-archivos-de-texto-plano}

Los archivos se guardan en el formato de "Org mode".


### Tipo de ubicación (repositorio) {#tipo-de-ubicación--repositorio}

Puede guardar cuadernos sincronizados en su dispositivo móvil, tarjeta SD o en Dropbox.

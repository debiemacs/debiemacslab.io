+++
title = "Java - Notas básicas"
date = 2020-03-13T19:08:00+01:00
lastmod = 2022-11-13T08:03:49+01:00
tags = ["Java"]
draft = false
+++

APUNTES CURSO JAVA


## Instanciar arreglos - sintaxix {#instanciar-arreglos-sintaxix}

```java
nombreArreglo = new tipo[Largo];
enteros = new int[10];
personas[1] = new Persona("Pedro","Laura");

tipo[] nombrearreglo = { valor1, valor2, ...};
```


## Instanciar matrices - sintaxis {#instanciar-matrices-sintaxis}

 #+BEGIN_SRC java
tipo [][] nombrematriz;
tipo nombrematriz [][];

nombrematriz = new tipo [][];
personas[1][1] = new Persona("Pedro","Lola");

int i = enteros [0][0];
Persona p2 = persona[1][0];

//----------------------------------------------------------------

for (int i= 0; i &lt; nombres.lenght; i++){

       for(int i=0; j &lt; nombre[i].lenght; j++){
            System.out.println("matriz" + nombres[i][j];
}
}
// String nombres[][] = {{"Luis","Pepe","Sandra"}};
//Los tres serían la fila "0", Luis, Pepe y Sandra, columnas 0,1,2.

\#+END_SRC java


## VISTO {#visto}


## REPASO - REALIZAR RESUMEN PROCEDIMIENTO {#repaso-realizar-resumen-procedimiento}


## Ejemplos sintaxis {#ejemplos-sintaxis}

```java
//1. Declaremos un arreglo de enteros
        int edades[];
        //2. Instanciar el arreglo de enteros
        edades = new int[3];
        //3. Inicializamos los valores del arreglo de enteros
        edades[0] = 30;
        edades[1] = 15;
        edades[2] = 20;

        //Imprimimos los valores del arreglo a la salida estandar
        //4. leemos cada valor del arreglo
        System.out.println("Arreglo enteros indice 0: " + edades[0]);
        System.out.println("Arreglo enteros indice 1: " + edades[1]);
        System.out.println("Arreglo enteros indice 2: " + edades[2]);

        //1.Declarar un arreglo de tipo object
        Persona personas[];
        //2. Instanciar el arreglo de tipo object
        personas = new Persona[4];
        //3. Inicializar los valores del arreglo
        personas[0] = new Persona("Juan");
        personas[1] = new Persona("Karla");

        //4. imprimir los valores del arreglo
        System.out.println("Arreglo personas indice 0: " + personas[0]);
        System.out.println("Arreglo personas indice 1: " + personas[1]);
        System.out.println("Arreglo personas indice 2: " + personas[2]);
        System.out.println("Arreglo personas indice 3: " + personas[3]);

        //Arreglo utilizando notacion simplificada
        String nombres[] = {"Sara", "Laura", "Carlos", "Carmen"};
        //imprimir los elementos de un arreglo
        for(int i=0; i < nombres.length ; i++){
            System.out.println("Arreglo String indice " + i + ": " + nombres[i]);
        }

    }
}
```


## JERARQUÍA DE CLASES Y HERENCIAS {#jerarquía-de-clases-y-herencias}

   Interfaces: Una coleccion de metodos que pueden ser usados por una clase, pero
son solo definidos no implementados.
PALABRAS CLAVE: interface y implements
Ejp:

```java
Interface Nave{
           public static final int VIDA = 100;
           public abstract void disparar();
           public abstract void moverposicion(int x, int y);
}
```

   Paquetes son modos de agrupar clases e interfaces relacionadas.
Para usarlas debemos importarlo a nuestra aplicacion.

```example
Ejp: java.awt;
     java.io;
```


## PALABRA FINAL {#palabra-final}

Creación de una constante, en variables evita cambiar el valo que almacena la variable.
Las constantes se utilizan en mayúsculas.

Podemos usarla en métodos. una subclase no puede modificar sólo utilizar el método con final.

En clases que se cree una subclase.
Math.PI

Ejemplo uso palabra FINAL


## HERENCIA {#herencia}

Palabra clave EXTENDS, hereda los métodos de la clase que extiende y éste tiene sus propios métodos,
pero los atributos con la palabra clave PRIVATE, no. La Herencia se restringe con los modificadores
de acceso en los atributos o métodos.

sintaxis.

class Persona{} //Hereda de object.

class Empleado extends Persona{} //Hereda Persona

class Gente extends Empleado{} // hereda todos los métodos y atributos de Empleado y Persona.
//simpre y cuando no sean private, éstos sólo los podrá usar su propia clase.


## CONSTRUCTORES {#constructores}

Es un tipo de metodo especial, NEW llama al metodo constructor de la clase.
No son obligatorios, se inicializara con los valores por defecto.
Es importante que la firma del constructor coincida con la que asignamos cuando creamos
con el metodo, tal como vemos en el ejemplo.


### Ejemplo code: {#ejemplo-code}

```java
    class Circulo{
int x, y, radio;

Circulo(int puntoX,int puntoY, int tamRadio){ //primer constructor con tres parametros
this.x = puntoX;
this.y = puntoY;
this.radio= tamRadio;
}
Circulo(int puntoX, int puntoY){ //Segundo constructor con dos parametros.
  this(puntoX, puntoY, 1);
}
void Resultado(){
  int resultado = x*y*radio;
  System.out.println(resultado);
}
public static void main(String[] arguments){
  Circulo circulo = new Circulo(2,3,4); //Creamos un objeto, importante que coincida en parametros con algun constructor.
  circulo.Resultado(); //llamada al metodo resultado para que nos muestre en pantalla el resultado.
}
}
```


## SOBREESCRITURA DE MÉTODOS {#sobreescritura-de-métodos}

Existe una Jerarquia de metodos,ya que hay una jerarquia de clases, definimos un metodo en nuestra  subclase con la misma firma
que un metodo de su superclase.


### Ejemplo code. {#ejemplo-code-dot}

```java
// Tenemos dos metodos iguales "imprimeme" tanto en la clase como en
//la sublclase, en el segundo metodo incluimos la variable z. Al ejecutar
//el programa el metodo que ejecutara sera el de la subclase ya que es el
//primero que encuentra.

class Imprimir{
int x= 0;
int y= 1;

void imprimeme (){
  System.out.println("x es " + x + " , y es " + y);
  System.out.println("Soy una instancia de clase " +
  this.getClass().getName());
}
}

class subImprimir extends Imprimir {//subImprimir es una subclase de la clase Imprimir

int z = 3;

void imprimeme (){
System.out.println("x es " + x + " , y es " + y +  " z es "+ z);
  System.out.println("Soy una instancia de clase " +
  this.getClass().getName());
}

public static void main (String[] arguments){
  subImprimir obj = new subImprimir(); //creamos una instancia
  obj.imprimeme();
}
}
```


## PALABRA CLAVE "SUPER" {#palabra-clave-super}

La utilizaremos para llamar al metodo original de la superClase, estando nosotros en una subclase, es muy similar a la
palabra clave "THIS", es un contenedor.


### SOBREESCRIBIR CONSTRUCTORES {#sobreescribir-constructores}

La palabra clave "SUPER" nos permite llamar a metodos constructores de la superclase.
La sintaxis para llamara al constructor seria:
super (argumentos)
La sintaxis para llamar a un metodo de la superclase seria:
super.nombremetodo(argumentos)

Debe ser la primera llamada en el constructor.


#### Ejemplo code: {#ejemplo-code}

```java
import java.awt.Point;
class NombrePunto extends Point //extiende de la superclase Point
{
  String nombre;

  NombrePunto(int x, int y, String nombre){
      super(x,y);  //Tiene que ser la primera declaracion sino da error.
      this.nombre = nombre;

  }
  public static void main (String[] arguments){
      NombrePunto np = new NombrePunto(5,5,"PuntoEncuentro");
  }
}
```


## MODIFICADORES {#modificadores}

Son palabras clave que añadimos a las definiciones.
Private, Public,static,protected,default controlan el acceso para
variables y metodos.

MODIFICADOR "FINAL" sirve para indicar que no puede ser cambiado, de una clase FINAL no pueden crearse subclases,
cuando se lo aplicamos a un metodo le indicamos que no puede ser sobreescrito y cuando se lo aplicamos a una
variable lo que indicamos es que no se puede cambiar en ningun caso su valor.

Las variables FINAL son como constantes en Java.


### MODIFICADOR "ABSTRACT" {#modificador-abstract}

Crea clases como si fueran contenedores, clase Megane, tenemos clase berlinca, coupe...


## CONTROLES DE ACCESO {#controles-de-acceso}

Un metodo sobreescrito no debe ser mas restrictivo que el original.

PUBLIC: Los metodos declarados PUBLIC en una superclase deben ser tambien PUBLIC en todas las subclases.
PROTECTED: Los metodos declarados PROTECTED en una superclase, deben ser PUBLIC o PROTECTED en sus subclases, no pueden ser PRIVATE
DEFAULT: Los metodos declarados sin control de acceso no pueden ser declarados como PRIVATE.


## METODOS ACCESOR {#metodos-accesor}

Se usan para dar acceso a variables PRIVATE.
Se crean uno para poder leer y otro para poder escribir. Se suele poner antes de la variable, set y get. Ejp: setCodigoPostal(int)


### Ejemplo: {#ejemplo}

```java
class logger{
    private String formato;
    public String getFormat(){
          return this.formato;
    }

    public void setFormat(String formato){
       if ((formato.equals("admin")) || (formato.equals( "OutKast"))) {
            this.formato = formato;
       }
    }
}
```


## VARIABLES Y METODOS STATIC {#variables-y-metodos-static}

Pueden ser accedidos usando el nombre de la clase seguido por un punto y el nombre de la variable o metodo. Ejp: Color.black
Nos permite crear metodos y variables de clase.


## EJEMPLO GENERAL: {#ejemplo-general}

```java
public class ContadorInstancias {
  private static int numInstancias = 0;

  protected static int getCuenta() //metodo accesor (leer o acceder), se declara PROTECTED, solo pueden acceder su clase y subclases.
  {
      return numInstancias;
  }

  private static void addInstancia(){
      numInstancias++;
  }

  ContadorInstancias(){ //Constructor
      ContadorInstancias.addInstancia(); //ejecuta el metodo addInstancia que hara que aumente en uno el valor de numInstancias.
                                          // estamos accediendo mediante un metodo accesor a una variable PRIVATE.
  }


  public static void main(String[] arguments){

      System.out.println ("Empezamos con " + ContadorInstancias.getCuenta()+ " instancias"); //accedemos a numInstancias con el
                                                                                              //metodo accesor.
      for (int i = 0; i < 500; ++i) //creamos 500 objetos creados.
          new ContadorInstancias();

      System.out.println( "Creadas " + ContadorInstancias.getCuenta()+ " instancias"); //Vemos el numero de objetos creados.
  }
}
```


## PAQUETES {#paquetes}

Un paquete puede contener un numero ilimitado de clases, sirven para organizarlas. Palabra clave IMPORT.
Tienen que estar dentro del mismo directorio.
Tiene que ser la primera declaracion del codigo.
Con PACKAGE decimos que esa clase forme parte del paquete, se declararia en primera opcion.


## MUNDOPC.JAVA {#mundopc-dot-java}

```java

public class MundoPC {

    public static void main(String args[]) {

        //creacion de computadora Toshiba
        Monitor monitorToshi = new Monitor("Toshiba", 13);
        Teclado tecladoToshi = new Teclado("bluetooth", "Toshiba");
        Raton ratonToshi = new Raton("bluetooth", "Toshiba");
        Computadora compuToshiba = new Computadora("Computadora Toshiba", monitorToshi, tecladoToshi, ratonToshi);

        //creacion de computadora dell
        Monitor monitorDell = new Monitor("Dell", 15);
        Teclado tecladoDell = new Teclado("usb", "Dell");
        Raton ratonDell = new Raton("usb", "Dell");
        Computadora compuDell = new Computadora("Computadora Dell", monitorDell, tecladoDell, ratonDell);

        //creacion de computadora armada
        Computadora compuArmada = new Computadora("Computadora Armada", monitorDell, tecladoToshi, ratonToshi);

        //Agregamos las computadoras a la orden
        Orden orden1 = new Orden();
        orden1.agregarComputadora(compuToshiba);
        orden1.agregarComputadora(compuDell);
        orden1.agregarComputadora(compuArmada);
        //Imprimimos la orden
        orden1.mostrarOrden();

        //Agregamos una segunda orden
        Orden orden2 = new Orden();
        orden2.agregarComputadora(compuArmada);
        orden2.agregarComputadora(compuDell);
        System.out.println("");
        orden2.mostrarOrden();
    }
}
```


## ORDEN.JAVA {#orden-dot-java}

```java

package com.gm.mundopc;

public class Orden {

    private final int idOrden;
    //Declaracion del arreglo de computadoras
    private final Computadora computadoras[];
    private static int contadorOrdenes;
    private int contadorComputadoras;
    //Definimos el maximo elementos del arreglo
    private static final int maxComputadoras = 10;

    public Orden() {//Este es el constructor que es llamado cuando se crea la nueva orden
// en MundoPC.java
        this.idOrden = ++contadorOrdenes;
        //Se instancia el arreglo de computadoras
        computadoras = new Computadora[maxComputadoras];
    }

    public void agregarComputadora(Computadora computadora) {
        //Si las computadoras agregadas no superan al máximo de computadoras
        //agregamos la nueva computadora
        if (contadorComputadoras < maxComputadoras) {
            //Agregamos la nueva computadora al arreglo
            //e incrementamos el contador de computadoras
            computadoras[contadorComputadoras++] = computadora;
        }
        else{
            System.out.println("Se ha superado el maximo de productos: " + maxComputadoras);
        }
    }

    public void mostrarOrden() {
        System.out.println("Orden #:" + idOrden);
        System.out.println("Computadoras de la orden #" + idOrden + ":");
        for (int i = 0; i < contadorComputadoras; i++) {
            System.out.println(computadoras[i]);
        }
    }
}
```


## COMPUTADORA.JAVA {#computadora-dot-java}

```java

package com.gm.mundopc;

public class Computadora {

    private int idComputadora;
    private String nombre;
    private Monitor monitor;
    private Teclado teclado;
    private Raton raton;
    private static int contadorComputadoras;

    //constructor vacio
    private Computadora() {
        idComputadora = ++contadorComputadoras;
    }

    //constructor que inicializa variables
    public Computadora(String nombre, Monitor monitor, Teclado teclado, Raton raton) {
        this();
        this.nombre = nombre;
        this.monitor = monitor;
        this.raton = raton;
        this.teclado = teclado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Monitor getMonitor() {
        return monitor;
    }

    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
    }

    public Teclado getTeclado() {
        return teclado;
    }

    public void setTeclado(Teclado teclado) {
        this.teclado = teclado;
    }

    public Raton getRaton() {
        return raton;
    }

    public void setRaton(Raton raton) {
        this.raton = raton;
    }

    @Override
    public String toString() {
        return "Computadora{ idComputadora=" + idComputadora + ", nombre=" + nombre + ", monitor=" + monitor + ", teclado=" + teclado + ", raton=" + raton + '}';
    }
 }
```


## MONITOR.JAVA {#monitor-dot-java}

```java

   package com.gm.mundopc;

//Creación de la clase
public class Monitor {
    //Declaración de variables
    private final int idMonitor;
    private String marca;
    private double tamaño;
    private static int contadorMonitores;

    private Monitor(){
         idMonitor = ++contadorMonitores;
   }

    //Constructor que inicializa las variables
    public Monitor(String marca, double tamaño) {
        this();
        this.marca = marca;
        this.tamaño = tamaño;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

      public double getTamaño() {
        return tamaño;
    }

    public void setTamaño(double tamaño) {
        this.tamaño = tamaño;
    }

    //Método que concatena las variables y regresa una cadena
    @Override
    public String toString() {
        return "Monitor{" + " idMonitor=" + idMonitor + ", marca=" + marca + ", tamaño=" + tamaño + '}';
    }

}
```


## RATON.JAVA {#raton-dot-java}

```java

package com.gm.mundopc;

public class Raton extends DispositivoEntrada {

    private final int idRaton;
    private static int contadorRatones;

    //constructor que inicializa las variables
    public Raton(String tipoEntrada, String marca) {
        super(tipoEntrada, marca);
        idRaton = ++contadorRatones;
    }

    @Override
    public String toString() {
        return "Raton{" + "idRaton=" + idRaton + ", " + super.toString();
     }
}
```


## TECLADO.JAVA {#teclado-dot-java}

```java

package com.gm.mundopc;

public class Teclado extends DispositivoEntrada {

    private final int idTeclado;
    private static int contadorTeclado;

    //constructor que inicializa las variables
    public Teclado(String tipoEntrada, String marca) {
        super(tipoEntrada, marca);
        idTeclado = ++contadorTeclado;
    }

    @Override
    public String toString() {
        return "Teclado{" + "idTeclado=" + idTeclado + ", " + super.toString();
    }
}
```


## DISPOSITIVOENTRADA.JAVA {#dispositivoentrada-dot-java}

```java

package com.gm.mundopc;

public class DispositivoEntrada {

    private String tipoDeEntrada;
    private String marca;

    //constructor que inicializa las variables
    public DispositivoEntrada(String tipoDeEntrada, String marca) {
        this.tipoDeEntrada = tipoDeEntrada;
        this.marca = marca;
    }

    public String getTipoDeEntrada() {
        return tipoDeEntrada;
    }

    public void setTipoDeEntrada(String tipoDeEntrada) {
        this.tipoDeEntrada = tipoDeEntrada;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Override
    public String toString() {
        return "DispositivoEntrada{ marca=" + marca + ", tipoDeEntrada=" + tipoDeEntrada + "}";
    }
    }
```


## AMPLIAR CLASES CON INTERFACES {#ampliar-clases-con-interfaces}

<https://youtu.be/3v7sndgy4h4>

Proveen plantillas de comportamiento, complementar el sistema de herencia simple.
Una interface Java es una coleccion de comportamientos abstractos que pueden ser
adoptados por cualquier clase sin ser heredados de una superclase.

Una interface solo contiene definiciones de metodos abstractos y constantes.

Una interface da solucion a los problemas que plantea a Java no contar con
herencias multiples.

Se usa la palabra clave implements.


## EJEMPLO {#ejemplo}

Ordena de menor a mayor segun cantidad de articulos.


### REGALOS.JAVA {#regalos-dot-java}

```java

import com.illasaron.paratienda.*;
public class Regalos{

  public static void main (String[] arguments){
      Escaparate almacen = new Escaparate();

       almacen.addArticulo("C01","ZAPATILLAS","9.9","150");
       almacen.addArticulo("C02","BOLSA","12.99","82");
       almacen.addArticulo("C03","SUDADERA","5.99","800");
       almacen.addArticulo("D01","CAMISETA","4.99","90");

       //llamamos al metodo sort para ordenar segun la cantidad
       almacen.sort();

       for (int i= 0; i < almacen.getSize(); i++){
           Articulos mostrar = (Articulos) almacen.getArticulo(i);
           System.out.println("Articulos ID: " + mostrar.getId()+
                   "\nNombre: " + mostrar.getNombre() +
                   "\nPrecio Detalle: " + mostrar.getDetalle() +
                   "\nPrecio: " + mostrar.getPrecio()+
                   "\nCantidad: "+ mostrar.getCantidad());
       }
  }
}

//*** ARTICULOS.JAVA
package com.illasaron.paratienda;
import java.util.*;

public class Articulos implements Comparable {

  private String id;
  private String nombre;
  private double detalle;
  private int cantidad;
  private double precio;

  //constructor
  Articulos(String idIn, String nombreIn, String detalleIn, String cantIn){
      id = idIn;
      nombre = nombreIn;
      detalle = Double.parseDouble(detalleIn);
      cantidad = Integer.parseInt(cantIn);
      //Se obtiene el precio del producto dependiendo de la cantidad.
      if (cantidad > 400)
          precio = detalle*.5D;
      else if (cantidad > 200)
          precio = detalle*.6D;
      else
          precio = detalle*.7D;
      precio = Math.floor(precio*100+.5)/100;
  }

  //El metodo compareto, es el que tiene la interface comparable
      //, lo que hace es comparar dos objetos de una clase, el objeto
      //actual y otro objeto que se le pasa, devolviendo un entero.
      //+1,0,-1, por encima, igual o debajo.


  public int compareTo( Object obj){
      Articulos temp = (Articulos)obj;
      if (this.precio < temp.precio)
          return 1;
      else if (this.precio > temp.precio)
          return -1;
      return 0;
  }

  //creamos los metodos accesores GET, para las variables privadas.
  public String getId(){
      return id;
  }
  public String getNombre(){
      return nombre;
  }
  public double getDetalle(){
      return detalle;
  }
  public int getCantidad(){
      return cantidad;
  }
  public double getPrecio(){
      return precio;
  }
}
//*** ESCAPARATE.JAVA
package com.illasaron.paratienda;
import java.util.*;

public class Escaparate {

  //instanciamos un objeto de la clase LinkedList
  private LinkedList catalogo = new LinkedList();

  public void addArticulo(String id, String nombre, String precio,
          String cantidad){
      Articulos art = new Articulos(id,nombre,precio,cantidad);
      catalogo.add(art);

  }

  public Articulos getArticulo(int i){
      return (Articulos) catalogo.get(i);
  }

  public int getSize(){
      return catalogo.size();
  }

  public void sort(){
      Collections.sort(catalogo);
  }

}
```


## FOREACH <span class="tag"><span class="Java">Java</span></span> {#foreach}

Solo tiene dos elementos a declarar.
Declaramos la variable que va a contener cada uno de los elementos y
posteriormente el arreglo que vamos a utilizar para iterar cada uno de
los elementos.

Si necesitamos saber el índice que se está iterando, no nos vale.

En el caso que usemos varargs este debe ser el último parámetro.

```java

public class EjemploEachVarArgs{

public static void main (String[] args) {

      imprimirNumerosForEach(14,22,45,67,5,433);
}
public static void imprimirNumerosForEach(int... numeros){

      for(int numero : numeros{
         System.out.println("El numero es:" + numero);

   }
}
```

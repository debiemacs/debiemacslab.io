+++
title = "Notas Git"
date = 2019-10-12
lastmod = 2022-11-01T22:47:38+01:00
tags = ["git"]
draft = false
+++

Git es un Version control system fundamental para coordinar el trabajo entre
varios desarrolladores, controlar los cambios realizados, quién los ha hecho
revertir, si hay algún problema....Un básico para el día a día.

{{< figure src="~/Documentos/ORG-FILES/BLOG/git.png" >}}

**Crear un repositorio nuevo**

```text
git init
```

**Realizar una copia del repositorio**

```text
git clone /path/to/repository //creamos una copia local del repositorio
```

**Descargar los últimos cambios**

```text
git pull
```

**Añadir ficheros al Head (add y commit)**

```text
git add .
git commit -m "Mensaje de commit"
```

Con **git commit -m "mensaje"** nos ahorramos entrar al editor.

**Envio de cambios**

Los cambios que hemos realizado anteriormente están en el HEAD de nuestro MASTER,
para enviarlos ejecutamos:

```text
git push origin master
```

Si estamos en nuestro directorio MASTER con **git push**, seria suficiente.

Para actualizar el repositorio local al commit mas nuevo,
(descargar de nuestro MASTER en Git), git pull.

Para fusionar dos ramas diferentes usaremos

```text
git merge branch
```

Si modificamos un archivo en nuestro local y realizamos un **git status** nos dirá
que el archivo ha sido modificado y que podemos revertir esos cambios,
para ello haríamos un:

```text
git checkout -- fichero
```

Al ejecutar el comando podremos ver cómo nuestro archivo en local aparece sin los
últimos cambios que habíamos realizado.

Si volvieramos a realizar un **git status** nos indicaría que no hay nada para
realizar commit.

Si realizamos una modificación en un archivo y queremos ver las diferencias que
existen, ejecutaríamos **git diff** ,nos muestra las líneas de código que hemos
modificado.

Nos faltaría agregarlo con **git add fichero**, para guardarlo ejecutaríamos
**git commit** aparecerá el editor, presionamos la letra **i** y podremos escribir
para dejar una descripción del cambio que hemos realizado, **w** para escribir y
**q** para salir.

Al salir, nos informará de las líneas que hemos modificado o añadido.

Si escribísemos **git log** veremos el snapshot del archivo antes de modificarlo.

**Ignorar parte de proyecto**

Se nos puede dar el caso de haber creado una carpeta con varios archivos y no
queramos que parte de ello se suba a nuestro entorno de trabajo.

Para ello creamos un archivo **.gitignore** y escribimos dentro el directorio que
no queremos que se suba.

Nos faltaría realizar un **git add .gitignore**

**Trabajar con diferentes versiones del proyecto**

Otra opción muy interesante es tener la posibilidad de poder trabajar con una
versión de nuestro trabajo principal.

Si ejecutamos **git branch** nos mostrará que solo tenemos un proyecto, nuestra
carpeta master.

Si queremos tener una versión alternativa ejecutamos **git branch nombre**, si
ejecutamos un **git branch** nos aparecerá tanto master como el que acabamos de crear.

Si nos queremos mover a la nueva versión del proyecto que hemos creado
ejecutaremos **git checkout nombre**.

Al volver a realizar un **git checkout master** observaremos como desaparece del
local todas las modificaciones que hemos realizado en la rama alternativa.

+++
title = "Configurando Syncthing"
date = 2020-02-25
lastmod = 2022-11-13T08:03:50+01:00
tags = ["emacs"]
draft = false
+++

Syncthing apareció como un vaso de agua fresca cuando me preguntaba cómo podía usar mi agenda org en el móvil y que estuviera
actualizada constantemente con mi portatil.

Ejecutamos en cada dispositivo Syncthing

El ordenador lo consideraremos dispositivo principal, por lo tanto habrá que copiar su ID al resto, en nuestro caso, solicitaremos
desde nuestro ordenador el código QR y lo leeremos con el móvil para Añadir el dispositivo.

Seleccionando en el móvil, la opción "Introductor" en el dispositivo que hemos dado de alta (luis-pc)nos informará de todos
los dispositivos que conoce, y nos conectaremos automáticamente a ellos.

De esta forma, como nuestro dispositivo principal (luis-pc)  estará conectado a todos nuestros dispositivos, y en cada uno de estos
marcaremos al principal como Introductor, todos nuestros dispositivos estarán conectados entre sí.

En el ordenador creamos una carpeta nueva donde irán los archivos a sincronizar, autorizamos al resto de dispositivos. En los dispositivos
nos aparecerá un mensaje de permiso para compartir, y le decimos qué carpeta vamos a elegir para que sea común la sincronización.

Reiniciamos

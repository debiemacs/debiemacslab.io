+++
title = "Org-Capture, configurando el órden"
date = 2022-11-01T22:44:00+01:00
lastmod = 2022-11-13T08:03:51+01:00
tags = ["emacs", "orgmode"]
draft = false
+++

{{< figure src="/ox-hugo/orglogo.png" >}}

Buscando cómo mejorar mi flujo de trabajo con Emacs y su Agenda, me encontré con una de las
opciones de configuración que más pueden ayudar a tal propósito.

Org-Capture, nos presenta una combinación de teclas en la cual podemos anotar ideas, tareas, citas
de una manera muy rápida, dejándonos la opción de alojarlas en un archivo "reubicar.org", para
después alojarla en el .org que nosotros creamos más adecuado.

El flujo de trabajo sería el siguiente:

Pulsamos la combinación C-c c , y nos aparecerá un menú a seleccionar.

{{< figure src="/ox-hugo/capture1.png" >}}

Según la elección, nos aparecerá una plantilla a rellenar y tres opciones.
Si pulsamos C-c C-w reubicaremos en el árbol/fichero que queramos.
Si pulsamos C-c C-c nos almacenará la nota en un el archivo reubicar.org, para posteriormente
posicionarnos en esa nota y reubicarla a nuestro .org final con C-c C-w.
Si pulsamos C-c C-s salimos, descartando la nota.

{{< figure src="/ox-hugo/capture2.png" >}}

Nuestro código para el Init.el sería el siguiente.

```text

; Refile on top of file max
(setq org-refile-use-outline-path 'file)
; use a depth level of 6 max
(setq org-outline-path-complete-in-steps nil)
(setq org-refile-targets
      '((org-agenda-files . (:maxlevel . 4))))

(use-package org-capture
  :ensure nil
  :after org
  :bind ("C-c c" . org-capture)
  :preface
  (defvar my/org-basic-task-template "* TODO %^{Tareas}
   :PROPERTIES:
   :Effort: %^{effort|1:00|0:05|0:15|0:30|2:00|4:00}
   :END:
    Capturado %<%Y-%m-%d %H:%M>" "Plantilla básica de tareas.")

  (defvar my/org-contacts-template "* %(org-contacts-template-name)
   :PROPERTIES:
   :EMAIL: %(org-contacts-template-email)
   :PHONE: %^{123-456-789}
   :HOUSE: %^{123-456-789}
   :ALIAS: %^{nuko}
   :NICKNAME: %^{Carlos M}
   :IGNORE:
   :NOTE: %^{NOTA}
   :ADDRESS: %^{Calle Ejemplo 1 2A, 28320, Pinto, Madrid, España}
   :BIRTHDAY: %^{yyyy-mm-dd}
   :END:" "Plantilla para org-contacts.")
  :custom
  (org-capture-templates
   `(

     ("c" "Contactos" entry (file+headline "/home/luis/Dropbox/ARCHIVOS_ORG/contactos.org" "Amigos"),
      my/org-contacts-template
      :empty-lines 1)
     ("m" "Nota Música" entry (file+headline "/home/luis/Dropbox/ARCHIVOS_ORG/reubicar.org" "Nota Música"),
      my/org-basic-task-template
      :empty-lines 1)

     ("n" "Nota Ajedrez" entry (file+headline "/home/luis/Dropbox/ARCHIVOS_ORG/reubicar.org" "Ajedrez"),
      my/org-basic-task-template
      :empty-lines 1)

     ("i" "Citas" entry (file "/home/luis/Dropbox/ARCHIVOS_ORG/Diario.org" ),
      "* cita con %? \n%T"
      :empty-lines 1)

     ("b" "Blog" entry (file+headline "/home/luis/Dropbox/ARCHIVOS_ORG/reubicar.org" "Blog"),
      my/org-basic-task-template
      :empty-lines 1)

     ("t" "Tarea" entry (file+headline "/home/luis/Dropbox/ARCHIVOS_ORG/reubicar.org" "Tareas"),
      my/org-basic-task-template
      :empty-lines 1))))
```

+++
title = "C++ Constructores"
date = 2020-03-17T17:30:00+01:00
lastmod = 2022-11-13T08:03:49+01:00
tags = ["Cpp"]
draft = false
+++

{{< figure src="/home/luis/Documentos/ORG-FILES/BLOG/cmasmas.png" >}}

La finalidad de los constructores no es otra que la de inicializar un objeto que
hemos creado, inicializar las variables de la clase.

La sobrecarga de un contructor dependerá de las múltiples combinaciones que
queramos en la inicialización de las variables de la función.

Tienen las siguientes características

-   No tienen valor de retorno.
-   Se llama igual que la clase.
-   Se invoca cuando se declara un objeto.

Podríamos decir que existen tres tipos de constructores, constructor por defecto,
constructor por parámetros y constructores de copia.

```nil

#include <iostream>
using namespace std;

class Fecha {
   int dia, mes, anyo
   public:
    Fecha();//constructor por defecto
    Fecha(int d, int m, int a);//constructor
    Fecha(const Fecha& F); //constructor de copia
}
//constructores
Fecha::Fecha(){
    dia = 1;
    mes = 1;
    anyo = 2000;
}

Fecha::Fecha(int d, int m int a) {
    dia = d;
    mes = m;
    anyo = a;
}

Fecha::Fecha(const Fecha& F){
    dia = F.dia;
    mes = F.mes;
    anyo = F.anyo;
}

int main(){
  Fecha f(10,5,2001), g(21,2,1999); //inicializamos dos objetos con el constructor
  //Fecha h(); //Existe una función h que devuelve una fecha.
  Fecha h; //Creo una fecha llamando al constructor.

  Fecha k = f;//constructor de copia v1
  Fecha k2(f);//constructor de copia v2
  f.incrementar();
  h.incrementar();
  g.incrementar();
}
```

**El constructor por defecto** , lo usamos cuando queremos inicializar un objeto pero
no especificamos ningun valor, en nuestro caso (día, mes y anyo), si no
especificamos ningún de esos valores el compilador nos dá un error porque no
encuentra los parámetros, está esperando que le pasáramos los tres valores para
inicializarlos.

Aquí es donde entra el constructor por defecto, en nuestro ejemplo, _Fecha()_, con
ello nos aseguramos siempre inicializar de una manera genérica un objeto.

Importante saber cómo llamar a éste constructor por defecto, si lo hicieramos
tal como _Fecha h()_ estaríamos llamando a una función, debemos hacer la llamada
con _Fecha h_ tal como reflejamos en el códido ejemplo.

**El constructor de copia**, sirve para asignar los datos de un determinado constructor
y tomarlos como  propios para el de copia.

En nuestro ejemplo podríamos utilizarlo para que la _Fecha k_ tome el valor inicial
de la _Fecha f_.

Podŕiamos invocarlo como _Fecha k = f_ directamente, o con _Fecha(const Fecha&amp; F)_

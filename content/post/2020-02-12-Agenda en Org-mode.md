+++
title = "Una Agenda con Org-mode"
date = 2020-02-12
lastmod = 2022-11-13T08:03:50+01:00
tags = ["orgmode", "emacs"]
draft = false
+++

Una de las grandes funcionalidades que nos proporciona Emacs y Org-Mode es el
modo Agenda. Con el modo Agenda vamos a poder programar nuestro día a día de una
manera muy productiva.

La idea general es disponer de varios archivos .org de diferente temática donde
vayamos reflejando nuestras tareas pendientes.

Para ello en nuestro archivo de configuración de Emacs, configuraremos nuestra
Lista Todo Global, algo así:

```text

(setq org-agenda-files (list "/home/luis/Documentos/ORG-FILES/NotasCasa.org>
                         "/home/luis/Documentos/ORG-FILES/BLOG/NotasUni.org>
                        "/home/luis/Documentos/ORG-FILES/BLOG/tareas_globales.org>
                        "/home/luis/Documentos/ORG-FILES/BLOG/Org_Blog.org>
                       "/home/luis/Documentos/ORG-FILES/BLOG/ApuntesClase.org>
```

Podríamos asignar también un directorio completo donde tener en cuenta todos
los archivos .org alojados.

```text
(setq org-agenda-files '("~/mis-archivos-org"))
```

Como opción podríamos añadir diferentes **palabras clave** con diferentes colores
para hacer más precisa su utilización y no basarnos únicamente en las palabras
clave TODO y DONE.

Importante tener en cuenta en la configuración la posición de **"|"**, ésta nos
delimita las palabras que usamos para dar por completada una tarea.

Aquí podéis encontrar los colores tanto para el fondo como para la fuente con
sus correspondientes códigos que soporta Emacs.

<http://www.raebear.net/computers/emacs-colors/>

```text

;;CONFIGURACION ORG MODE-- PALABRAS CLAVE
(setq org-todo-keywords
'((sequence "TODO" "BORRADOR" "TRABAJANDO" "|" "DONE" "PUBLICADA" "POSPUESTO")))


;;CONFIGURACION ORG MODE-- COLORES PALABRAS CLAVE
(setq org-todo-keyword-faces
'(
("TODO"   . (:background "firebrick2" :weight bold))
("BORRADOR"  . (:foreground:"gray0" :background "orange red" :weight bold))
("DONE"   . (:foreground "gray0":background "green2" :weight bold))
("TRABAJANDO"   . (:foreground "navy" :background "azure2" :weight bold))
("PUBLICADA"   . (:foreground "gray0":background "green" :weight bold))
("POSPUESTO"  . (:foreground "gray0":background "grey70" :weight bold))
))
```

Configuraremos también las asignaciones más comunes para la Agenda,

Con **C-cl** creamos un link. Con **C-ca** llamamos al menú Agenda.

Con C-cc nos aparece org-capture, muy útil para tomar notas al vuelo y más adelante
clasificar, lo cerraríamos con **C-c C-c** volviendo automáticamente al buffer donde
estábamos.

```text
;; AGENDA
;;-------
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
```

**Usos más frecuentes con Org-Agenda**

Con **C-c C-s** abrimos el calendario, seleccionando la fecha programaremos la tarea
donde estemos.

Abriremos Org-Agenda con **C-c a**, pulsando **a** nos muestra la agenda semanal, si
invocamos al comando con un prefijo numérico nos mostrará el número de días que
hemos seleccionado.

```text
C-u 20 C-c-a a ;;nos mostrará la agenda en los siguientes 20 días
```

-   **C-c a T** Podremos introducir una palabra clave para que sólo se nos muestren
    éstas.

-   **C-c a m** Nos mostrará las tareas con unas determinadas etiquetas.

-   **C-c a M** Igual que la anterior pero con la condición que esas tareas sean un
    TODO.

-   **C-c a t** Nos mostará todos los TODO pendientes.

-   **C-c a s** Buscaremos una cadena de texto en todos los items.

-   **C-c a n** Nos muestra tanto la planificación semanal como los TODO pendientes.

Si qureremos programar unas tareas que se repitan durante un determinado tiempo,(horas, días, meses, años) y que cuando demos por DONE una tarea no nos de por finalizada las siguientes, deberemos configurarlo de esta manera.
\#+begin_src


## T+++ [#B] Práctica libre 30' {#t-plus-plus-plus-b-práctica-libre-30}

SCHxxxxxD: &lt;2022-11-xx mié ++1d&gt;

\#+end_src

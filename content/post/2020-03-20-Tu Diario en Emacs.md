+++
title = "Tu Diario en Emacs"
date = 2020-03-20
lastmod = 2022-11-13T08:03:49+01:00
tags = ["emacs"]
draft = false
+++

Una de las utilidades más sobresalientes que podemos emplear para intentar
ser más productivos es el uso de un Diario, con un diario vamos a poder reflejar
asiduamente nuestras tareas.

LLevando un diario de nuestras tareas conseguiremos además de ser más productivos
no desanimarnos ya que tendremos todo organizado, orden y productividad van
siempre ligados.

Emacs nos proporciona un Diario al uso muy fácil de configurar y de usar, que
junto a la herramienta calendar nos permitirá organizarnos mejor.

**Creamos el diario**
Todo se gestiona desde el fichero **_diary_**, sino lo tenemos creado en nuestro
directorio usuario, lo creamos desde nuestro terminal.

```text
$ touch ~/diary
```

**Añadimos entradas a nuestro diario**

Primero lanzamos el calendario con:

```text
M-x calendar
```

Una vez abierto podemos desplazarnos a una fecha X con:

```text
g d :'calendar-goto-date;
```

Seleccionamos la fecha deseada y usamos una de las siguientes combinaciones para
añadir entradas:

```text
i d : 'insert-diary-entry'; añade una entrada diaria
i w : 'insert-weekly-diary-entry'; añade una entrada semanal
i m : 'insert-monthly-diary-entry'; añade una entrada mensual
i y : 'insert-yearly-diary-entry'; añade una entrada anual
i a : 'insert-anniversary-diary-entry'; añade un aniversario
```

**Mostrar entradas del diario**

Dos maneras, pulsando **d** en Calendar o pulsando **s** si queremos que muestre
todas las entradas.

Ya lo tenemos!!!

+++
title = "Un Dashboard molón en Emacs"
date = 2022-11-01T22:44:00+01:00
lastmod = 2022-11-13T08:03:51+01:00
tags = ["emacs"]
draft = false
+++

Estas cosas a mí me gustan, que la pantalla de presentación de Emacs cuando inicias te muestre esta pinta..Uff!!
Me gusta!!

{{< figure src="/ox-hugo/dashboard.png" >}}

```text

(use-package dashboard
  :preface
    (setq dashboard-banner-logo-title (concat "GNU Emacs " emacs-version
  					  " kernel " (car (split-string (shell-command-to-string "uname -r") "-"))
  					  " x86_64 " (car (split-string (shell-command-to-string "/usr/bin/sh -c '. /etc/os-release && echo $PRETTY_NAME'") "\n"))))
  :init
    (add-hook 'after-init-hook 'dashboard-refresh-buffer)
  :custom (dashboard-set-file-icons t)
  :custom (dashboard-startup-banner 'logo)
  :config (dashboard-setup-startup-hook))
```

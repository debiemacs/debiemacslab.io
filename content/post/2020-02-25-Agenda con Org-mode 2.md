+++
title = "Agenda con Org-mode - 2"
date = 2022-10-25T11:23:00+02:00
lastmod = 2022-11-13T08:03:50+01:00
tags = ["emacs", "orgmode"]
draft = false
+++

Con éste código damos un paso más pro en nuestra Agenda.

```text
 (use-package org-agenda
      :ensure nil
      :after org
     ; :bind ([f5] . org-agenda)
      :custom
       (org-agenda-dim-blocked-tasks t)
       (org-agenda-inhibit-startup t)
       (org-agenda-show-log t)
       (org-agenda-files
       ; '( "/home/luis/Documentos/ARCHIVOS_ORG"))
         '( "/home/luis/Dropbox/ARCHIVOS_ORG"))
       (org-agenda-skip-deadline-if-done t)
       (org-agenda-skip-deadline-prewarning-if-scheduled 'pre-scheduled)
       (org-agenda-skip-scheduled-if-done t)
       (org-agenda-start-on-weekday 1)
       (org-agenda-sticky nil)
       (org-agenda-window-setup 'current-window) ;Sobrescribe la ventana actual con la agenda
       (org-agenda-tags-column -100)
       (org-agenda-time-grid '((daily today require-timed)))
       (org-agenda-use-tag-inheritance t)
       (org-enforce-todo-dependencies t)
       (org-habit-show-habits-only-for-today nil)
       (org-track-ordered-property-with-tag t)
       (org-icalendar-timezone "Europe/Madrid")
       (calendar-day-name-array ["domingo" "lunes" "martes" "miércoles"
                                  "jueves" "viernes" "sábado"])
       (calendar-month-name-array ["enero" "febrero" "marzo" "abril" "mayo"
                                    "junio" "julio" "agosto" "septiembre"
                                    "octubre" "noviembre" "diciembre"])
  (org-agenda-custom-commands
      '(("h" "Habitos" tags-todo "STYLE=\"habit\""
         ((org-agenda-overriding-header "Habitos")
          (org-agenda-sorting-strategy
           '(todo-state-down effort-up category-keep))))
              ("o" "Estudio"
          ((agenda "" )
             (tags-todo "estudio/INICIADA"
                   ((org-agenda-overriding-header "Tareas Iniciadas")
                    (org-tags-match-list-sublevels t)))
             (tags-todo "estudio/SIGUIENTE"
                        ((org-agenda-overriding-header "Siguientes Tareas")
                    (org-tags-match-list-sublevels t)))
             (tags-todo "estudio/TODO"
                   ((org-agenda-overriding-header "Tareas por Hacer")
                    (org-tags-match-list-sublevels t)))
             (tags "estudio/CANCELADA"
                   ((org-agenda-overriding-header "Tareas Canceladas")
                    (org-tags-match-list-sublevels t)))
             (tags "estudio/DONE"
                   ((org-agenda-overriding-header "Tareas terminadas sin Archivar")
                    (org-tags-match-list-sublevels t)))
             (tags "reubicar"
                   ((org-agenda-overriding-header "Reubicar")
                    (org-tags-match-list-sublevels t)))
             nil))
      ;; Reportes personales
      ("p"  "Personal"
          ((agenda "" )
             (tags-todo "personal/INICIADA"
                   ((org-agenda-overriding-header "Tareas Iniciadas")
                    (org-tags-match-list-sublevels t)))
             (tags-todo "personal/SIGUIENTE"
                   ((org-agenda-overriding-header "Siguientes Tareas")
                    (org-tags-match-list-sublevels t)))
             (tags-todo "personal/TODO"
                   ((org-agenda-overriding-header "Tareas por Hacer")
                    (org-agenda-todo-ignore-deadlines 'future)
                    (org-agenda-todo-ignore-scheduled 'future)
                    (org-agenda-tags-todo-honor-ignore-options t)
                    (org-tags-match-list-sublevels t)))
             (tags "personal/CANCELADA"
                   ((org-agenda-overriding-header "Tareas Canceladas")
                    (org-tags-match-list-sublevels t)))
             (tags "personal/PUBLICADA"
                   ((org-agenda-overriding-header "Tareas terminadas sin Archivar")
                    (org-tags-match-list-sublevels t)))
             (tags "reubicar"
                   ((org-agenda-overriding-header "Reubicar")
                    (org-tags-match-list-sublevels t)))
             nil))
)))
```

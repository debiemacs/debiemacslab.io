+++
lastmod = 2023-01-21T08:19:21+01:00
draft = false
+++

## <span class="org-todo done PUBLICADA">PUBLICADA</span> Guía básica de Orgzly <span class="tag"><span class="orgmode">orgmode</span><span class="orgzly">orgzly</span></span> {#2019-10-12-Guía básica de Orgzly}

Orgzly es una aplicación disponible para Android, mediante Fdroid. Nos va a
permitir crear, editar nuestras notas Org desde nuestro móvil, yo la
utilizo junto a otra gran apliación Synthing, con esta logro sincronizar en
diferentes dispositivos todo mi mundo Org.

Dejo la explicación de funcionamiento oficial, me quedará pendiente editar
y realizar una explicación más clara y particular de todas sus funcionalidades.

---

¡Bienvenido a Orgzly!


### Notas {#notas}


#### Haga clic en la nota para seleccionarla {#haga-clic-en-la-nota-para-seleccionarla}

Se pueden seleccionar múltiples notas.


#### Plegar una nota que contiene subnotas haciendo clic en el icono a su lado {#plegar-una-nota-que-contiene-subnotas-haciendo-clic-en-el-icono-a-su-lado}

<!--list-separator-->

-  No hay límite en el número de niveles que se pueden tener

    <!--list-separator-->

    -  Utilice esto para organizar mejor sus notas


#### Una nota puede tener etiquetas                               " <span class="tag"><span class="tag1">tag1</span><span class="tag2">tag2</span></span> {#una-nota-puede-tener-etiquetas}

<!--list-separator-->

-  Las subnotas heredan las etiquetas <span class="tag"><span class="tag3">tag3</span></span>

    Si usted busca notas con la etiqueta "tag1", esta nota también se devolverá.
    "


#### Una nota puede tener un estado {#una-nota-puede-tener-un-estado}

Puede configurar cualquier número de estados a utilizar: TODO, NEXT, DONE, etc.

<!--list-separator-->

-  Hay dos _tipos_ de estados: to-do y done

<!--list-separator-->

- <span class="org-todo done DONE">DONE</span>  Esta es una nota con el tipo de estado _done_


#### Una nota puede tener un momento de inicio programado {#una-nota-puede-tener-un-momento-de-inicio-programado}

<!--list-separator-->

-  La fecha puede ser una repetición


#### También se puede establecer un tiempo límite {#también-se-puede-establecer-un-tiempo-límite}


#### Se soportan los recordatorios para horas programadas y de tiempo límite {#se-soportan-los-recordatorios-para-horas-programadas-y-de-tiempo-límite}

Estan inhabilitadas por defecto y necesitan estar habilitadas en Configuración.


#### Una nota puede tener una prioridad {#una-nota-puede-tener-una-prioridad}

Puedes cambiar el número de prioridades en Configuración. También puedes cambiar la prioridad por defecto - la prioridad asumida para notas sin una puesta.


#### Una nota puede incluir enlaces {#una-nota-puede-incluir-enlaces}

Marcar un número de teléfono (tel:555-0199), enviar SMS (sms:555-0199), componer un correo electrónico (<mailto:support@orgzly.com>) o visitar una página web ([Orgzly.com](http://www.orgzly.com)).

También puede enlazar a otra nota o bloc de notas dentro de la aplicación.

Consulte <http://www.orgzly.com/help#links> para obtener más información.


#### Están soportados formatos tipográficos básicos {#están-soportados-formatos-tipográficos-básicos}

Puede escribir palabras en **negrita**, _italica_, <span class="underline">subrayadas</span>, = cita literal=, `código` y ~~tachadas~~.


#### Se pueden crear listas de verificación {#se-pueden-crear-listas-de-verificación}

-   [X] Tarea 1
-   [ ] Tarea 2
-   [ ] Tarea 3

Pulsa la caja de verificación para alternar su estado. Pulsa el botón de nueva línea al final de la línea para crear un nuevo eleme


### Buscar {#buscar}


#### Existen muchos operadores de búsqueda soportados {#existen-muchos-operadores-de-búsqueda-soportados}

Puede buscar notas por estado, etiqueta, fecha de inicio, fecha límite, etc.

Visite <http://www.orgzly.com/help#search> para aprender más.


#### Las consultas se pueden guardar como accesos rápidos {#las-consultas-se-pueden-guardar-como-accesos-rápidos}

Pruebe los ejemplos de búsqueda del cajón de navegación y observe las consultas
que utilizan.

Puede crear sus propias búsquedas y guardarlas haciendo clic en "Búsquedas" en
el cajón de navegación.


### Sincronizando {#sincronizando}


#### Los cuadernos pueden guardarse como archivos de texto plano {#los-cuadernos-pueden-guardarse-como-archivos-de-texto-plano}

Los archivos se guardan en el formato de "Org mode".


#### Tipo de ubicación (repositorio) {#tipo-de-ubicación--repositorio}

Puede guardar cuadernos sincronizados en su dispositivo móvil, tarjeta SD o en Dropbox.


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Notas Git <span class="tag"><span class="git">git</span></span> {#2019-10-12-Básico GIT}

Git es un Version control system fundamental para coordinar el trabajo entre
varios desarrolladores, controlar los cambios realizados, quién los ha hecho
revertir, si hay algún problema....Un básico para el día a día.

{{< figure src="~/Documentos/ORG-FILES/BLOG/git.png" >}}

**Crear un repositorio nuevo**

```text
git init
```

**Realizar una copia del repositorio**

```text
git clone /path/to/repository //creamos una copia local del repositorio
```

**Descargar los últimos cambios**

```text
git pull
```

**Añadir ficheros al Head (add y commit)**

```text
git add .
git commit -m "Mensaje de commit"
```

Con **git commit -m "mensaje"** nos ahorramos entrar al editor.

**Envio de cambios**

Los cambios que hemos realizado anteriormente están en el HEAD de nuestro MASTER,
para enviarlos ejecutamos:

```text
git push origin master
```

Si estamos en nuestro directorio MASTER con **git push**, seria suficiente.

Para actualizar el repositorio local al commit mas nuevo,
(descargar de nuestro MASTER en Git), git pull.

Para fusionar dos ramas diferentes usaremos

```text
git merge branch
```

Si modificamos un archivo en nuestro local y realizamos un **git status** nos dirá
que el archivo ha sido modificado y que podemos revertir esos cambios,
para ello haríamos un:

```text
git checkout -- fichero
```

Al ejecutar el comando podremos ver cómo nuestro archivo en local aparece sin los
últimos cambios que habíamos realizado.

Si volvieramos a realizar un **git status** nos indicaría que no hay nada para
realizar commit.

Si realizamos una modificación en un archivo y queremos ver las diferencias que
existen, ejecutaríamos **git diff** ,nos muestra las líneas de código que hemos
modificado.

Nos faltaría agregarlo con **git add fichero**, para guardarlo ejecutaríamos
**git commit** aparecerá el editor, presionamos la letra **i** y podremos escribir
para dejar una descripción del cambio que hemos realizado, **w** para escribir y
**q** para salir.

Al salir, nos informará de las líneas que hemos modificado o añadido.

Si escribísemos **git log** veremos el snapshot del archivo antes de modificarlo.

**Ignorar parte de proyecto**

Se nos puede dar el caso de haber creado una carpeta con varios archivos y no
queramos que parte de ello se suba a nuestro entorno de trabajo.

Para ello creamos un archivo **.gitignore** y escribimos dentro el directorio que
no queremos que se suba.

Nos faltaría realizar un **git add .gitignore**

**Trabajar con diferentes versiones del proyecto**

Otra opción muy interesante es tener la posibilidad de poder trabajar con una
versión de nuestro trabajo principal.

Si ejecutamos **git branch** nos mostrará que solo tenemos un proyecto, nuestra
carpeta master.

Si queremos tener una versión alternativa ejecutamos **git branch nombre**, si
ejecutamos un **git branch** nos aparecerá tanto master como el que acabamos de crear.

Si nos queremos mover a la nueva versión del proyecto que hemos creado
ejecutaremos **git checkout nombre**.

Al volver a realizar un **git checkout master** observaremos como desaparece del
local todas las modificaciones que hemos realizado en la rama alternativa.


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Notas EMACS <span class="tag"><span class="emacs">emacs</span></span> {#2019-10-12-Notas Emacs}

En este post voy reflejando diferentes configuraciones que hago uso en Emacs,
aquí encontraremos un pequeño cajón desastre.


### SELECCIONAR/COPIAR/PEGAR {#seleccionar-copiar-pegar}

```text
//Ctrl + Espace // Ctrl + w // Ctrl + y//
```


### CHULETA EMACS {#chuleta-emacs}

```text
Mover por páginas

Ctrl v – (siguiente)
Alt v – (anterior)
Ctrl l – Limpiar la pantalla y volver a mostrar todo el texto, moviendo el texto alrededor del cursor al centro de la pantalla.
Moverse entre el texto

Ctrl f – Moverse un caracter hacia adelante
Ctrl b – Moverse un caracter hacia atrás
Alt f – Moverse una palabra hacia adelante
Alt b – Moverse una palabra hacia atrás

Ctrl n – Moverse a la siguiente línea
Ctrl p – Moverse a la línea anterior
Ctrl a – Moverse al comienzo de una línea
Ctrl e – Moverse al final de una línea
Alt a – Moverse al inicio de una oración
Alt e – Moverse al final de una oración
Alterando el texto

Ctrl k – kill, «mata» el texto. Esto implica que lo borra, pero lo mantiene guardado y puede ser recuperado con Ctrl Y.
Ctrl d – Borra un caracter (borra, no mata, no puede ser recuperado).
Alt d – Borra palabras

Ctrl @ / Ctrl Espacio – marca el texto (primera marca)
Ctrl w – corto texto desde la marca de texto.
Alt w – copia texto desde la marca de texto.
Ctrl h – Marcar todo el buffer, como «Seleccionar todo».

Ctrl y – «Yanks text», pega el texto matado o cortado/copiado con w.
Alt y – Recorre yanks previos, podemos recuperar algo que matamos varios Ctrl k antes.

Ctrl g – Cancelar comandos
Ctrl x u – Deshacer
Ctrl / – Deshacer
Ctrl x – Comandos

Varios comandos se realizan presionando Ctrl x y después una tecla, o Ctrl una tecla. Por ejemplo el «deshacer» de las líneas anteriores, se tiene que presionar Ctrl x (soltar) y presionar u.

Ctrl x Ctrl f – «Visitar» nuevo archivo (si existe lo abre, sino lo crea).
Ctrl g – Cancelar comandos

Buffers

Ctrl x Ctrl b – Listar buffers. En Emacs no hay «pestañas» o «ventanas». Cada archivo se abre en un buffer. Con este comando vemos todos los buffers que hayan abiertos en esta sesión de Emacs.
Ctrl x b – Cambiar de buffer. Muy práctico, podemos usar el tabulador para autocompletar el nombre de un buffer.
Ctrl x k – Cerrar buffer
Ctrl x Ctrl s – Guardar un archivo
Ctrl x s – Guardar buffers

Ctrl X Ctrl C – Salir de Emacs
Ctrl z – Suspende la sesion de emacs. Volvemos desde la consola con fg

Alt x – Modo comandos. Nos permite ejecutar comandos como los siguientes:
Alt x replace-string – reemplazo de strings
Alt x recover file – levantar respaldo del archivo

Ctrl S – Buscar strings incrementalmente (con Ctrl S sigo buscando hacia adelante, con Ctrl R busco hacia atrás y con enter termino la busqueda)
Alt % buscar ENTER reemplazar – Buscar y reemplazar.

Ctrl x 2 – Divide la ventana en 2 de forma horizontal.
Ctrl x 3 – Divide la ventana en 2 de manera vertical.
Ctrl x 1 – Deja solo una ventana abierta.
Ctrl Alt v – scrollea la ventana donde no tengo el foco.
Ctrl x o – cambia el cursor de una ventana a otra.
```


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Un maravilloso mundo con Org-Mode <span class="tag"><span class="orgmode">orgmode</span><span class="emacs">emacs</span></span> {#un-maravilloso-mundo-con-org-mode}

CLOSED: <span class="timestamp-wrapper"><span class="timestamp">[2020-03-13 vie 19:10]</span></span>

:EXPORT_FILE_NAME: 2019-10-14-Notas Org_mode
:EXPORT_DATE: 2019-10-14

Qué voy a decir de Org-Mode!!

{{< figure src="/home/luis/Documentos/ORG-FILES/BLOG/OXHUGO.png" >}}

Mil posibilidades para poder organizarte, tener todo ordenado, ser más
productivo....Org-Mode te brinda múltiples herramientas para organizar
tu día a día....y todo dentro de Emacs!!!

Dejo mi **"chuleta"** particular de las cosas que uso.


### ATAJOS SENCILLOS {#atajos-sencillos}

```text
TAB sobre un encabezado, oculta o muestra el contenido

Shift - TAB muestra todo el conjunto

C-c C-t Atajo TODO/DONE

Shift-TAB En cualquier posicion, oculta o muestra el contenido de todos los
encabezados.

C-c C-n ir al siguiente encabezado

C-c C-p ir al encabezado anterior

C-c C-f ir al siguiente encabezado del mismo nivel

C-c C-b ir al encabezado anterior del mismo nivel

C-c C-u ir al encabezado padre

C-c C-q insertar etiquetas

M-FlechaDerecha agregar un "*" al encabezado

M-FlechaIzquierda quitar un "*" al encabezado

M-FlechaArriba Si el encabezado superior es del mismo nivel, intercambia el
lugar de los encabezados, es decir mueve el actual hacia arriba.
```


### RESALTADO FUENTES {#resaltado-fuentes}

```text
*bold* ==> bold
~code~ ==> code
+strike+ ==> strike
/italic/ ==> italic
=verbatim= ==> code
_underline_ ==> underline
```


### IMÁGENES INLINE {#imágenes-inline}

En el encabezado inicializamos siempre el documento con:

```text
#+STARTUP:inlineimages
```

Incluimos la ruta en la zona del documento que queramos con dobles corchetes
tanto al comienzo como al final.

```text
[[file:rutax\rutaxx\fichero.png]]
```

Pulsando la combinación C-c C-x C-v mostraremos o ocultaremos todas las imágenes
del documento.


### ENLACES {#enlaces}

En principio usamos links sencillos, con añadirlo directamente es suficiente, nos
lo implementará automaticamente.

```text
https:\\debian.es
```


#### Enlaces soportados {#enlaces-soportados}

```text
http://www.astro.uva.nl/~dominik          en la red
doi:10.1000/182                           DOI para un recurso electronico
file:/home/dominik/images/jupiter.jpg     fichero, ruta absoluta
/home/dominik/images/jupiter.jpg          igual que arriba
file:papers/last.pdf                      fichero, ruta relativa
./papers/last.pdf                         igual que arriba
file:/myself@some.where:papers/last.pdf   fichero, ruta a una maquina remota
/myself@some.where:papers/last.pdf        igual que arriba
file:sometextfile::NNN                    fichero, saltar al numero de linea
file:projects.org                         otro fichero Org
file:projects.org::some words             buscar texto en fichero Org[fn:35]
file:projects.org::*task title            buscar encabezado en fichero Org[fn:36]
file+sys:/path/to/file                    abrir via OS, como doble-click
file+emacs:/path/to/file                  fuerza apertura con Emacs
docview:papers/last.pdf::NNN              abrir en modo doc-view en la pagina
id:B7423F4D-2E8A-471B-8810-C40F074717E9   Enlazar con ecabezado por ID
news:comp.emacs                           enlace Usenet
mailto:adent@galaxy.net                   enlace Mail
mhe:folder                                enlace a carpeta MH-E
mhe:folder#id                             enlace a mensage MH-E
rmail:folder                              enlace a carpeta RMAIL
rmail:folder#id                           enlace a mensaje RMAIL
gnus:group                                enlace a grupo Gnus
gnus:group#id                             enlace a articulo Gnus
bbdb:R.*Stallman                          enlace BBDB (con regexp)
irc:/irc.com/#emacs/bob                   enlace IRC
info:org#External links                   enlace a nodo o indice Info
shell:ls *.org                            Un comando shell
elisp:org-agenda                          Comando Interactivo Elisp
elisp:(find-file-other-frame "Elisp.org") formate de evaluacion Elisp
```


### CHECKBOXES {#checkboxes}

\#+begin_example


#### Ejemplo {#ejemplo}

-   <code>[50%]</code> ESTUDIAR
    -   [X] Coger El LIBRO
    -   [ ] PRIMERA LECCION
-   [X] HACERLOS

\#+end_example

Con Ctrol C C chequearemos una casilla.

Si al crearla añadimos <code>[%]</code> o <code>[/]</code> nos mostrará tanto por ciento realizado.


### BLOQUES {#bloques}

Podemos introducir bloques y nos lo adaptará al formato que queramos.
Diferentes tipos:

```text
EXAMPLE ejemplos
HTML seccion de codigo a exportar tal cual
LaTex seccion de codigo a exportar tal cual
SRC bloque de codigo
QUOTE bloque de cita
```

En el caso de SRC añadimos el lenguaje que queremos que refleje: c, java, php...

```java

package com.gm.mundopc;

public class Raton extends DispositivoEntrada {

    private final int idRaton;
    private static int contadorRatones;

    //constructor que inicializa las variables
    public Raton(String tipoEntrada, String marca) {
        super(tipoEntrada, marca);
        idRaton = ++contadorRatones;
    }

    @Override
    public String toString() {
        return "Raton{" + "idRaton=" + idRaton + ", " + super.toString();
     }
}
```


### EXPORTAR {#exportar}

Exportamos con la combinación C-c , C-e , pero antes añadimos en el enbabezado
del documento:

Para colocar imagenes dentro del contenido que exportes simplemente coloca la

direccion de la imagen, asi:

```text
[[[[file:mi-imagen.png]]]]
```

```text
#+OPTIONS: toc:nil num:nil h:7 html-preamble:nil html-postamble:nil html-scripts:nil html-style:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="worg-data/worg.css" />
```


### LISTA TODO GLOBAL {#lista-todo-global}

Una de las más interesantes que suelo usar, podemos combinar diferentes archivos org
para gestionar de un vistazo las tareas pendientes:

Añadimos en nuestro archivo de configuración Emacs:

```text
(setq org-agenda-files (list "~/org/trabajo.org"
                             "~/org/clase.org"
                             "~/org/casa.org"))
```

Al presionar C-c a t , org-mode escaneará todas las tareas pendients en los
archivos que hayamos configurado en list y nos lo mostrará.

Pulsando t , hacemos un DONE. Con INTRO vamos directamente a esa tarea.


### PLANIFICANDO TAREAS CON LA AGENDA {#planificando-tareas-con-la-agenda}

Añadir a nuestro archivo emacs de configuración esto:

```text
;; Las siguiente líneas son siempre necesarias. Elige
;; tus propias claves
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)
```

C-c C-s aparece org-Schedule, y vamos el calendario, elegimos fecha.
Org-mode inserta una marca después del TODO.

C-c a a para org-agenda. Se mostrará una vista con los items planificados.


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Un blog con Hugo y Gitlab - Parte 1 <span class="tag"><span class="orgmode">orgmode</span><span class="git">git</span><span class="emacs">emacs</span><span class="hugo">hugo</span></span> {#2019-10-14-Crear_un_blog_1}

Cuando me planteé crear un blog personal de anotaciones y de apuntes,
tenía varias cosas muy claras, quería un blog sin publicidad, que fuera sencillo
y que toda la carga de trabajo la pudiera realizar desde Emacs.

Al principio lo realicé con ORG-Page, y lo cierto es que funcionaba muy bien,
pero mis escasos conocimientos de CSS, y la manera tan particular que tenía
ORG-Page de gestionar los temas de las plantillas, hicieron que finalmente lo
desechara, cierto es que lo logré configurar muy correctamente, estéticamente
como me gustaba, pero el hecho de no saber realmente cómo trabajaba internamente
me creaba una sensación agridulce en el control del blog.

No quiero decir que ORG-Page sea difícil, o menos válido que cualquier otra
opción, sólo que yo no fuí capaz de sentirme cómodo totalmente. Le podéis echar
un vistazo si queréis, <http://encodigoemacs.gitlab.io> , os dejo también como
referencia el del compañero Notxor creado integramente con ORG-Page, un blog muy
bueno, <https://notxor.nueva-actitud.org>.

Dentro de las diferentes opciones que hay, la razón que me hizo decantarme
por Hugo, fue que aunque trabaja con el contenido en Markdown, tienes la
posibilidad con otras herramientas de poder seguir editando en ORG y Emacs,
también la gran cantidad de plantillas que existían para temas era un aliciente,
ya que no quería perder demasiado el tiempo en ello.

La herramienta que permite ese nexo de unión entre Hugo y Org-Mode es OX-Hugo,
fue un gran descrubimiento que me vino del excelente blog
<http:%5C%5Celblogdelazaro.gitlab.io> , él integra también Hugo con OX-hugoobteniendo
un excelente resultado.

En este ciclo de posts voy a "intentar" explicar cómo realizo éste blog, paso a
paso.

Vayamos por partes, lo primero consistirá en crear nuestro espacio de trabajo en
<https:%5C%5Cgitlab.com> , Gitlab nos permitirá con sus propias herramientas generar
una plantilla principal de Hugo, obteniendo en pocos pasos un blog totalmente
operativo.


### Creamos un Grupo {#creamos-un-grupo}

{{< figure src="\IMAGEN_1_BLOG.png" >}}

Rellenamos todos los datos, seleccionando el Nivel de visibilidad en Público
y seleccionamos CREATE GROUP.

{{< figure src="\IMAGEN_0_1_BLOG.png" >}}


### Creamos el Proyecto {#creamos-el-proyecto}

Ahora ya tenemos creado y seleccionado un Grupo de Trabajo, lo que haremos
ahora será crear un proyecto, para ello seleccionamos dentro de nuestro
grupo recién creado Nuevo Proyecto.

{{< figure src="\IMAGEN_0_BLOG.png" >}}


### Creamos proyecto inicial HUGO {#creamos-proyecto-inicial-hugo}

Seleccionamos crear desde una plantilla y seleccionamos HUGO.

{{< figure src="\IMAGEN_2_BLOG.png" >}}


### Clonamos a Local nuestro nuevo Proyecto {#clonamos-a-local-nuestro-nuevo-proyecto}

GitLab nos habrá generado todo el árbol de archivos y directorios necesarios
del blog que hemos creado, nos habrá creado el blog con una plantilla genérica y
sólo nos quedará clonarlo a nuestro equipo para poder gestionar todas las
modificaciones que hagamos de una manera cómoda.

{{< figure src="\IMAGEN_3_BLOG.png" >}}

Para clonarlo pulsaremos la pestaña Clonar, copiaremos la ruta que nos dé y
ejecutamos en nuestro equipo:

\#+begin_src bash
git clone xxxxxxxxxxxxxxx
\#+end_src code


### Ejecutamos Pipelines {#ejecutamos-pipelines}

Vamos en el menú de la izquierda a la sección CI/CD Pipelines y pulsamos
ejecutar Pipeline.

{{< figure src="\IMAGEN_4_BLOG.png" >}}

En la sección Configuración ---Pages nos aparecerá la ruta del blog creado,
en unos minutos lo tendremos operativo.

{{< figure src="\IMAGEN_5_BLOG.png" >}}

Por último iremos a Configuración----General----Avanzado----Cambiar la Ruta,
introducimos una ruta más directa...nombre_proyecto.gitlab.io

{{< figure src="\IMAGEN_6_BLOG.png" >}}

Esperamos unos minutos a que se genere de nuevo el blog y tendremos ya creado
un blog con una plantilla genérica tanto en la web como en nuestra máquina local.

{{< figure src="\IMAGEN_7_BLOG.png" >}}


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Java - Notas básicas <span class="tag"><span class="Java">Java</span></span> {#2020-02-05-Java Conceptos Básicos}

APUNTES CURSO JAVA


### Instanciar arreglos - sintaxix {#instanciar-arreglos-sintaxix}

```java
nombreArreglo = new tipo[Largo];
enteros = new int[10];
personas[1] = new Persona("Pedro","Laura");

tipo[] nombrearreglo = { valor1, valor2, ...};
```


### Instanciar matrices - sintaxis {#instanciar-matrices-sintaxis}

 #+BEGIN_SRC java
tipo [][] nombrematriz;
tipo nombrematriz [][];

nombrematriz = new tipo [][];
personas[1][1] = new Persona("Pedro","Lola");

int i = enteros [0][0];
Persona p2 = persona[1][0];

//----------------------------------------------------------------

for (int i= 0; i &lt; nombres.lenght; i++){

       for(int i=0; j &lt; nombre[i].lenght; j++){
            System.out.println("matriz" + nombres[i][j];
}
}
// String nombres[][] = {{"Luis","Pepe","Sandra"}};
//Los tres serían la fila "0", Luis, Pepe y Sandra, columnas 0,1,2.

\#+END_SRC java

**\* VISTO
\*** REPASO - REALIZAR RESUMEN PROCEDIMIENTO


### Ejemplos sintaxis {#ejemplos-sintaxis}

```java
//1. Declaremos un arreglo de enteros
        int edades[];
        //2. Instanciar el arreglo de enteros
        edades = new int[3];
        //3. Inicializamos los valores del arreglo de enteros
        edades[0] = 30;
        edades[1] = 15;
        edades[2] = 20;

        //Imprimimos los valores del arreglo a la salida estandar
        //4. leemos cada valor del arreglo
        System.out.println("Arreglo enteros indice 0: " + edades[0]);
        System.out.println("Arreglo enteros indice 1: " + edades[1]);
        System.out.println("Arreglo enteros indice 2: " + edades[2]);

        //1.Declarar un arreglo de tipo object
        Persona personas[];
        //2. Instanciar el arreglo de tipo object
        personas = new Persona[4];
        //3. Inicializar los valores del arreglo
        personas[0] = new Persona("Juan");
        personas[1] = new Persona("Karla");

        //4. imprimir los valores del arreglo
        System.out.println("Arreglo personas indice 0: " + personas[0]);
        System.out.println("Arreglo personas indice 1: " + personas[1]);
        System.out.println("Arreglo personas indice 2: " + personas[2]);
        System.out.println("Arreglo personas indice 3: " + personas[3]);

        //Arreglo utilizando notacion simplificada
        String nombres[] = {"Sara", "Laura", "Carlos", "Carmen"};
        //imprimir los elementos de un arreglo
        for(int i=0; i < nombres.length ; i++){
            System.out.println("Arreglo String indice " + i + ": " + nombres[i]);
        }

    }
}
```


### JERARQUÍA DE CLASES Y HERENCIAS {#jerarquía-de-clases-y-herencias}

   Interfaces: Una coleccion de metodos que pueden ser usados por una clase, pero
son solo definidos no implementados.
PALABRAS CLAVE: interface y implements
Ejp:

```java
Interface Nave{
           public static final int VIDA = 100;
           public abstract void disparar();
           public abstract void moverposicion(int x, int y);
}
```

   Paquetes son modos de agrupar clases e interfaces relacionadas.
Para usarlas debemos importarlo a nuestra aplicacion.

```example
Ejp: java.awt;
     java.io;
```


### PALABRA FINAL {#palabra-final}

Creación de una constante, en variables evita cambiar el valo que almacena la variable.
Las constantes se utilizan en mayúsculas.

Podemos usarla en métodos. una subclase no puede modificar sólo utilizar el método con final.

En clases que se cree una subclase.
Math.PI

Ejemplo uso palabra FINAL


### HERENCIA {#herencia}

Palabra clave EXTENDS, hereda los métodos de la clase que extiende y éste tiene sus propios métodos,
pero los atributos con la palabra clave PRIVATE, no. La Herencia se restringe con los modificadores
de acceso en los atributos o métodos.

sintaxis.

class Persona{} //Hereda de object.

class Empleado extends Persona{} //Hereda Persona

class Gente extends Empleado{} // hereda todos los métodos y atributos de Empleado y Persona.
//simpre y cuando no sean private, éstos sólo los podrá usar su propia clase.


### CONSTRUCTORES {#constructores}

Es un tipo de metodo especial, NEW llama al metodo constructor de la clase.
No son obligatorios, se inicializara con los valores por defecto.
Es importante que la firma del constructor coincida con la que asignamos cuando creamos
con el metodo, tal como vemos en el ejemplo.


#### Ejemplo code: {#ejemplo-code}

```java
    class Circulo{
int x, y, radio;

Circulo(int puntoX,int puntoY, int tamRadio){ //primer constructor con tres parametros
this.x = puntoX;
this.y = puntoY;
this.radio= tamRadio;
}
Circulo(int puntoX, int puntoY){ //Segundo constructor con dos parametros.
    this(puntoX, puntoY, 1);
}
void Resultado(){
    int resultado = x*y*radio;
    System.out.println(resultado);
}
public static void main(String[] arguments){
    Circulo circulo = new Circulo(2,3,4); //Creamos un objeto, importante que coincida en parametros con algun constructor.
    circulo.Resultado(); //llamada al metodo resultado para que nos muestre en pantalla el resultado.
}
}
```


### SOBREESCRITURA DE MÉTODOS {#sobreescritura-de-métodos}

Existe una Jerarquia de metodos,ya que hay una jerarquia de clases, definimos un metodo en nuestra  subclase con la misma firma
que un metodo de su superclase.


#### Ejemplo code. {#ejemplo-code-dot}

```java
// Tenemos dos metodos iguales "imprimeme" tanto en la clase como en
//la sublclase, en el segundo metodo incluimos la variable z. Al ejecutar
//el programa el metodo que ejecutara sera el de la subclase ya que es el
//primero que encuentra.

class Imprimir{
int x= 0;
int y= 1;

void imprimeme (){
    System.out.println("x es " + x + " , y es " + y);
    System.out.println("Soy una instancia de clase " +
    this.getClass().getName());
}
}

class subImprimir extends Imprimir {//subImprimir es una subclase de la clase Imprimir

int z = 3;

void imprimeme (){
System.out.println("x es " + x + " , y es " + y +  " z es "+ z);
    System.out.println("Soy una instancia de clase " +
    this.getClass().getName());
}

public static void main (String[] arguments){
    subImprimir obj = new subImprimir(); //creamos una instancia
    obj.imprimeme();
}
}
```


### PALABRA CLAVE "SUPER" {#palabra-clave-super}

La utilizaremos para llamar al metodo original de la superClase, estando nosotros en una subclase, es muy similar a la
palabra clave "THIS", es un contenedor.


#### SOBREESCRIBIR CONSTRUCTORES {#sobreescribir-constructores}

La palabra clave "SUPER" nos permite llamar a metodos constructores de la superclase.
La sintaxis para llamara al constructor seria:
super (argumentos)
La sintaxis para llamar a un metodo de la superclase seria:
super.nombremetodo(argumentos)

Debe ser la primera llamada en el constructor.

<!--list-separator-->

-  Ejemplo code:

    ```java
    import java.awt.Point;
    class NombrePunto extends Point //extiende de la superclase Point
    {
        String nombre;

        NombrePunto(int x, int y, String nombre){
            super(x,y);  //Tiene que ser la primera declaracion sino da error.
            this.nombre = nombre;

        }
        public static void main (String[] arguments){
            NombrePunto np = new NombrePunto(5,5,"PuntoEncuentro");
        }
    }
    ```


### MODIFICADORES {#modificadores}

Son palabras clave que añadimos a las definiciones.
Private, Public,static,protected,default controlan el acceso para
variables y metodos.

MODIFICADOR "FINAL" sirve para indicar que no puede ser cambiado, de una clase FINAL no pueden crearse subclases,
cuando se lo aplicamos a un metodo le indicamos que no puede ser sobreescrito y cuando se lo aplicamos a una
variable lo que indicamos es que no se puede cambiar en ningun caso su valor.

Las variables FINAL son como constantes en Java.


#### MODIFICADOR "ABSTRACT" {#modificador-abstract}

Crea clases como si fueran contenedores, clase Megane, tenemos clase berlinca, coupe...


### CONTROLES DE ACCESO {#controles-de-acceso}

Un metodo sobreescrito no debe ser mas restrictivo que el original.

PUBLIC: Los metodos declarados PUBLIC en una superclase deben ser tambien PUBLIC en todas las subclases.
PROTECTED: Los metodos declarados PROTECTED en una superclase, deben ser PUBLIC o PROTECTED en sus subclases, no pueden ser PRIVATE
DEFAULT: Los metodos declarados sin control de acceso no pueden ser declarados como PRIVATE.


### METODOS ACCESOR {#metodos-accesor}

Se usan para dar acceso a variables PRIVATE.
Se crean uno para poder leer y otro para poder escribir. Se suele poner antes de la variable, set y get. Ejp: setCodigoPostal(int)


#### Ejemplo: {#ejemplo}

```java
class logger{
    private String formato;
    public String getFormat(){
          return this.formato;
    }

    public void setFormat(String formato){
       if ((formato.equals("admin")) || (formato.equals( "OutKast"))) {
            this.formato = formato;
       }
    }
}
```


### VARIABLES Y METODOS STATIC {#variables-y-metodos-static}

Pueden ser accedidos usando el nombre de la clase seguido por un punto y el nombre de la variable o metodo. Ejp: Color.black
Nos permite crear metodos y variables de clase.


### EJEMPLO GENERAL: {#ejemplo-general}

```java
public class ContadorInstancias {
    private static int numInstancias = 0;

    protected static int getCuenta() //metodo accesor (leer o acceder), se declara PROTECTED, solo pueden acceder su clase y subclases.
    {
        return numInstancias;
    }

    private static void addInstancia(){
        numInstancias++;
    }

    ContadorInstancias(){ //Constructor
        ContadorInstancias.addInstancia(); //ejecuta el metodo addInstancia que hara que aumente en uno el valor de numInstancias.
                                            // estamos accediendo mediante un metodo accesor a una variable PRIVATE.
    }


    public static void main(String[] arguments){

        System.out.println ("Empezamos con " + ContadorInstancias.getCuenta()+ " instancias"); //accedemos a numInstancias con el
                                                                                                //metodo accesor.
        for (int i = 0; i < 500; ++i) //creamos 500 objetos creados.
            new ContadorInstancias();

        System.out.println( "Creadas " + ContadorInstancias.getCuenta()+ " instancias"); //Vemos el numero de objetos creados.
    }
}
```


### PAQUETES {#paquetes}

Un paquete puede contener un numero ilimitado de clases, sirven para organizarlas. Palabra clave IMPORT.
Tienen que estar dentro del mismo directorio.
Tiene que ser la primera declaracion del codigo.
Con PACKAGE decimos que esa clase forme parte del paquete, se declararia en primera opcion.


### MUNDOPC.JAVA {#mundopc-dot-java}

```java

public class MundoPC {

    public static void main(String args[]) {

        //creacion de computadora Toshiba
        Monitor monitorToshi = new Monitor("Toshiba", 13);
        Teclado tecladoToshi = new Teclado("bluetooth", "Toshiba");
        Raton ratonToshi = new Raton("bluetooth", "Toshiba");
        Computadora compuToshiba = new Computadora("Computadora Toshiba", monitorToshi, tecladoToshi, ratonToshi);

        //creacion de computadora dell
        Monitor monitorDell = new Monitor("Dell", 15);
        Teclado tecladoDell = new Teclado("usb", "Dell");
        Raton ratonDell = new Raton("usb", "Dell");
        Computadora compuDell = new Computadora("Computadora Dell", monitorDell, tecladoDell, ratonDell);

        //creacion de computadora armada
        Computadora compuArmada = new Computadora("Computadora Armada", monitorDell, tecladoToshi, ratonToshi);

        //Agregamos las computadoras a la orden
        Orden orden1 = new Orden();
        orden1.agregarComputadora(compuToshiba);
        orden1.agregarComputadora(compuDell);
        orden1.agregarComputadora(compuArmada);
        //Imprimimos la orden
        orden1.mostrarOrden();

        //Agregamos una segunda orden
        Orden orden2 = new Orden();
        orden2.agregarComputadora(compuArmada);
        orden2.agregarComputadora(compuDell);
        System.out.println("");
        orden2.mostrarOrden();
    }
}
```


### ORDEN.JAVA {#orden-dot-java}

```java

package com.gm.mundopc;

public class Orden {

    private final int idOrden;
    //Declaracion del arreglo de computadoras
    private final Computadora computadoras[];
    private static int contadorOrdenes;
    private int contadorComputadoras;
    //Definimos el maximo elementos del arreglo
    private static final int maxComputadoras = 10;

    public Orden() {//Este es el constructor que es llamado cuando se crea la nueva orden
// en MundoPC.java
        this.idOrden = ++contadorOrdenes;
        //Se instancia el arreglo de computadoras
        computadoras = new Computadora[maxComputadoras];
    }

    public void agregarComputadora(Computadora computadora) {
        //Si las computadoras agregadas no superan al máximo de computadoras
        //agregamos la nueva computadora
        if (contadorComputadoras < maxComputadoras) {
            //Agregamos la nueva computadora al arreglo
            //e incrementamos el contador de computadoras
            computadoras[contadorComputadoras++] = computadora;
        }
        else{
            System.out.println("Se ha superado el maximo de productos: " + maxComputadoras);
        }
    }

    public void mostrarOrden() {
        System.out.println("Orden #:" + idOrden);
        System.out.println("Computadoras de la orden #" + idOrden + ":");
        for (int i = 0; i < contadorComputadoras; i++) {
            System.out.println(computadoras[i]);
        }
    }
}
```


### COMPUTADORA.JAVA {#computadora-dot-java}

```java

package com.gm.mundopc;

public class Computadora {

    private int idComputadora;
    private String nombre;
    private Monitor monitor;
    private Teclado teclado;
    private Raton raton;
    private static int contadorComputadoras;

    //constructor vacio
    private Computadora() {
        idComputadora = ++contadorComputadoras;
    }

    //constructor que inicializa variables
    public Computadora(String nombre, Monitor monitor, Teclado teclado, Raton raton) {
        this();
        this.nombre = nombre;
        this.monitor = monitor;
        this.raton = raton;
        this.teclado = teclado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Monitor getMonitor() {
        return monitor;
    }

    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
    }

    public Teclado getTeclado() {
        return teclado;
    }

    public void setTeclado(Teclado teclado) {
        this.teclado = teclado;
    }

    public Raton getRaton() {
        return raton;
    }

    public void setRaton(Raton raton) {
        this.raton = raton;
    }

    @Override
    public String toString() {
        return "Computadora{ idComputadora=" + idComputadora + ", nombre=" + nombre + ", monitor=" + monitor + ", teclado=" + teclado + ", raton=" + raton + '}';
    }
 }
```


### MONITOR.JAVA {#monitor-dot-java}

```java

   package com.gm.mundopc;

//Creación de la clase
public class Monitor {
    //Declaración de variables
    private final int idMonitor;
    private String marca;
    private double tamaño;
    private static int contadorMonitores;

    private Monitor(){
         idMonitor = ++contadorMonitores;
   }

    //Constructor que inicializa las variables
    public Monitor(String marca, double tamaño) {
        this();
        this.marca = marca;
        this.tamaño = tamaño;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

      public double getTamaño() {
        return tamaño;
    }

    public void setTamaño(double tamaño) {
        this.tamaño = tamaño;
    }

    //Método que concatena las variables y regresa una cadena
    @Override
    public String toString() {
        return "Monitor{" + " idMonitor=" + idMonitor + ", marca=" + marca + ", tamaño=" + tamaño + '}';
    }

}
```


### RATON.JAVA {#raton-dot-java}

```java

package com.gm.mundopc;

public class Raton extends DispositivoEntrada {

    private final int idRaton;
    private static int contadorRatones;

    //constructor que inicializa las variables
    public Raton(String tipoEntrada, String marca) {
        super(tipoEntrada, marca);
        idRaton = ++contadorRatones;
    }

    @Override
    public String toString() {
        return "Raton{" + "idRaton=" + idRaton + ", " + super.toString();
     }
}
```


### TECLADO.JAVA {#teclado-dot-java}

```java

package com.gm.mundopc;

public class Teclado extends DispositivoEntrada {

    private final int idTeclado;
    private static int contadorTeclado;

    //constructor que inicializa las variables
    public Teclado(String tipoEntrada, String marca) {
        super(tipoEntrada, marca);
        idTeclado = ++contadorTeclado;
    }

    @Override
    public String toString() {
        return "Teclado{" + "idTeclado=" + idTeclado + ", " + super.toString();
    }
}
```


### DISPOSITIVOENTRADA.JAVA {#dispositivoentrada-dot-java}

```java

package com.gm.mundopc;

public class DispositivoEntrada {

    private String tipoDeEntrada;
    private String marca;

    //constructor que inicializa las variables
    public DispositivoEntrada(String tipoDeEntrada, String marca) {
        this.tipoDeEntrada = tipoDeEntrada;
        this.marca = marca;
    }

    public String getTipoDeEntrada() {
        return tipoDeEntrada;
    }

    public void setTipoDeEntrada(String tipoDeEntrada) {
        this.tipoDeEntrada = tipoDeEntrada;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Override
    public String toString() {
        return "DispositivoEntrada{ marca=" + marca + ", tipoDeEntrada=" + tipoDeEntrada + "}";
    }
    }
```


### AMPLIAR CLASES CON INTERFACES {#ampliar-clases-con-interfaces}

<https://youtu.be/3v7sndgy4h4>

Proveen plantillas de comportamiento, complementar el sistema de herencia simple.
Una interface Java es una coleccion de comportamientos abstractos que pueden ser
adoptados por cualquier clase sin ser heredados de una superclase.

Una interface solo contiene definiciones de metodos abstractos y constantes.

Una interface da solucion a los problemas que plantea a Java no contar con
herencias multiples.

Se usa la palabra clave implements.


### EJEMPLO {#ejemplo}

Ordena de menor a mayor segun cantidad de articulos.


#### REGALOS.JAVA {#regalos-dot-java}

```java

import com.illasaron.paratienda.*;
public class Regalos{

    public static void main (String[] arguments){
        Escaparate almacen = new Escaparate();

         almacen.addArticulo("C01","ZAPATILLAS","9.9","150");
         almacen.addArticulo("C02","BOLSA","12.99","82");
         almacen.addArticulo("C03","SUDADERA","5.99","800");
         almacen.addArticulo("D01","CAMISETA","4.99","90");

         //llamamos al metodo sort para ordenar segun la cantidad
         almacen.sort();

         for (int i= 0; i < almacen.getSize(); i++){
             Articulos mostrar = (Articulos) almacen.getArticulo(i);
             System.out.println("Articulos ID: " + mostrar.getId()+
                     "\nNombre: " + mostrar.getNombre() +
                     "\nPrecio Detalle: " + mostrar.getDetalle() +
                     "\nPrecio: " + mostrar.getPrecio()+
                     "\nCantidad: "+ mostrar.getCantidad());
         }
    }
}

//*** ARTICULOS.JAVA
package com.illasaron.paratienda;
import java.util.*;

public class Articulos implements Comparable {

    private String id;
    private String nombre;
    private double detalle;
    private int cantidad;
    private double precio;

    //constructor
    Articulos(String idIn, String nombreIn, String detalleIn, String cantIn){
        id = idIn;
        nombre = nombreIn;
        detalle = Double.parseDouble(detalleIn);
        cantidad = Integer.parseInt(cantIn);
        //Se obtiene el precio del producto dependiendo de la cantidad.
        if (cantidad > 400)
            precio = detalle*.5D;
        else if (cantidad > 200)
            precio = detalle*.6D;
        else
            precio = detalle*.7D;
        precio = Math.floor(precio*100+.5)/100;
    }

    //El metodo compareto, es el que tiene la interface comparable
        //, lo que hace es comparar dos objetos de una clase, el objeto
        //actual y otro objeto que se le pasa, devolviendo un entero.
        //+1,0,-1, por encima, igual o debajo.


    public int compareTo( Object obj){
        Articulos temp = (Articulos)obj;
        if (this.precio < temp.precio)
            return 1;
        else if (this.precio > temp.precio)
            return -1;
        return 0;
    }

    //creamos los metodos accesores GET, para las variables privadas.
    public String getId(){
        return id;
    }
    public String getNombre(){
        return nombre;
    }
    public double getDetalle(){
        return detalle;
    }
    public int getCantidad(){
        return cantidad;
    }
    public double getPrecio(){
        return precio;
    }
}
//*** ESCAPARATE.JAVA
package com.illasaron.paratienda;
import java.util.*;

public class Escaparate {

    //instanciamos un objeto de la clase LinkedList
    private LinkedList catalogo = new LinkedList();

    public void addArticulo(String id, String nombre, String precio,
            String cantidad){
        Articulos art = new Articulos(id,nombre,precio,cantidad);
        catalogo.add(art);

    }

    public Articulos getArticulo(int i){
        return (Articulos) catalogo.get(i);
    }

    public int getSize(){
        return catalogo.size();
    }

    public void sort(){
        Collections.sort(catalogo);
    }

}
```


### FOREACH <span class="tag"><span class="Java">Java</span></span> {#foreach}

Solo tiene dos elementos a declarar.
Declaramos la variable que va a contener cada uno de los elementos y
posteriormente el arreglo que vamos a utilizar para iterar cada uno de
los elementos.

Si necesitamos saber el índice que se está iterando, no nos vale.

En el caso que usemos varargs este debe ser el último parámetro.

```java

public class EjemploEachVarArgs{

public static void main (String[] args) {

      imprimirNumerosForEach(14,22,45,67,5,433);
}
public static void imprimirNumerosForEach(int... numeros){

      for(int numero : numeros{
         System.out.println("El numero es:" + numero);

     }
}

```


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Un blog con Hugo y Gitlab - Parte 2 <span class="tag"><span class="emacs">emacs</span><span class="hugo">hugo</span><span class="orgmode">orgmode</span></span> {#2020-03-13-Crear_un_blog_2}

En esta segunda parte mostraré cómo tengo configurado Emacs con OX-Hugo para
crear posts en el blog que tenemos creado con Hugo.

La idea principal consiste en crear un archivo .ORG único donde iremos alojando
los diferentes posts que vayamos creando, seguidamente con la ayuda de OX-Hugo
generaremos de una manera automática los archivos Mark Down alojándolos en la
carpeta "Post" de nuestro proyecto Gitlab.

Una vez generado el nuevo post realizaremos un commit mediante Git, con ello
publicando el nuevo post.


### Configurando Emacs {#configurando-emacs}

Instalamos Ox-hugo

```text

M-x package-install RET ox-hugo RET

```

En nuestro init.el o .emacs añadiremos las siguientes líneas:

```text

(with-eval-after-load 'ox__
  (require 'ox-hugo))

```

Utilizaremos un único archivo .org para generar los post, con una configuración
general y una configuración básica que aplicaremos a cada post.

Como ejemplo mi archivo .org:

{{< figure src="/ox-hugo/unblogconhugoparte2.png" >}}


### Exportando el artículo a Mark Down {#exportando-el-artículo-a-mark-down}

Para ello usaremos la herramienta de Org-Mode ejecutando C-c - C-e, con H-H
exportaremos el post donde estemos situados, con H-A exportaríamos todos nuestros
posts.

{{< figure src="/ox-hugo/unblogconhugoparte2imagen2.png" >}}


### Realizamos un commit a Gitlab {#realizamos-un-commit-a-gitlab}

Para ello podríamos usar alguna herramienta desde el propio Emacs, yo de momento
uso directamente la terminal desde Emacs, nos posicionamos en nuestro directorio
git y ejecutamos:

```text

git add .
git commit -m "post1"
git push

```


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Irc con Irssi <span class="tag"><span class="terminal">terminal</span></span> {#2020-03-14-Irc con Irssi}

Siguen resistiendo los canales Irc al Whatsapp, Telegram, Matrix.., sobre todo
para temas técnicos y comunicaciones directas vienen muy bien, en Irc muy rara
vez te encontrarás un off topic, se respira máxima seriedad.

Tenemos Erc para Emacs funcionando muy bien pero a mí personalmente me gusta
tener por separado la ejecución de Irc, y para ello no hay nada mejor que Irssi.

Irssi es una aplicación de consola que te va a permitir un control total de los
canales y servidores, fácil y sin distraciones.

Una vez instalado Irssi pasaremos directamente a configurar el config, lo
encontraremos en nuestro home, directorio oculto .irssi.

Configuración Inicio automático:

```text
/network add mired //Creamos una red por si usamos diferentes servidores.

/server add -4 -auto -ircnet mired irc.freenode.net 6667
```

Algunos comandos básicos:

```text
/server list
/channel list
/msg usuario <mensaje> //mensaje privado
/nick <nuevo nick>
/exit //desconectamos del servidor
/whois <usuario>
/channel //nos muestra información del canal

```

Os muestro mi archivo de configuración con las partes más relevantes.

```nil
servers = (
  {
    address = "irc.freenode.net";
    chatnet = "mired";
    port = "6667";
    password = "XXXX";
    use_tls = "no";
    tls_verify = "no";
    autoconnect = "yes";
  }
);

chatnets = {

  Freenode = {
    type = "IRC";
    max_kicks = "1";
    max_msgs = "4";
    max_whois = "1";
  };

  mired = { type = "IRC"; };
};

//Configuraremos los canales que queremos que se autoejecuten

channels = (
  { name = "#emacs"; chatnet = "mired"; autojoin = "Yes"; },
  { name = "#emacs-es"; chatnet = "mired"; autojoin = "Yes"; },
  { name = "#debian"; chatnet = "mired"; autojoin = "Yes"; },
  { name = "#debian-es"; chatnet = "mired"; autojoin = "Yes"; },
  { name = "#libreplanet"; chatnet = "mired"; autojoin = "Yes"; },
  { name = "#ircsource"; chatnet = "IRCSource"; autojoin = "No"; },
  { name = "#netfuze"; chatnet = "NetFuze"; autojoin = "No"; },
  { name = "#oftc"; chatnet = "OFTC"; autojoin = "No"; },
  { name = "silc"; chatnet = "SILC"; autojoin = "No"; }
);


};

//Configuramos información básica, nuestro Nick y el mensaje de despedida.
settings = {
  core = {
    real_name = "luis";
    user_name = "luis";
    nick = "Mi_nick";
    quit_message = "I'll be back soon guys";
  };
  "fe-text" = { actlist_sort = "refnum"; };
};

```


## <span class="org-todo done PUBLICADA">PUBLICADA</span> C++ Constructores <span class="tag"><span class="Cpp">Cpp</span></span> {#2020-03-17-C++ Constructores}

{{< figure src="/home/luis/Documentos/ORG-FILES/BLOG/cmasmas.png" >}}

La finalidad de los constructores no es otra que la de inicializar un objeto que
hemos creado, inicializar las variables de la clase.

La sobrecarga de un contructor dependerá de las múltiples combinaciones que
queramos en la inicialización de las variables de la función.

Tienen las siguientes características

-   No tienen valor de retorno.
-   Se llama igual que la clase.
-   Se invoca cuando se declara un objeto.

Podríamos decir que existen tres tipos de constructores, constructor por defecto,
constructor por parámetros y constructores de copia.

```nil

#include <iostream>
using namespace std;

class Fecha {
   int dia, mes, anyo
   public:
    Fecha();//constructor por defecto
    Fecha(int d, int m, int a);//constructor
    Fecha(const Fecha& F); //constructor de copia
}
//constructores
Fecha::Fecha(){
    dia = 1;
    mes = 1;
    anyo = 2000;
}

Fecha::Fecha(int d, int m int a) {
    dia = d;
    mes = m;
    anyo = a;
}

Fecha::Fecha(const Fecha& F){
    dia = F.dia;
    mes = F.mes;
    anyo = F.anyo;
}

int main(){
  Fecha f(10,5,2001), g(21,2,1999); //inicializamos dos objetos con el constructor
  //Fecha h(); //Existe una función h que devuelve una fecha.
  Fecha h; //Creo una fecha llamando al constructor.

  Fecha k = f;//constructor de copia v1
  Fecha k2(f);//constructor de copia v2
  f.incrementar();
  h.incrementar();
  g.incrementar();
}

```

**El constructor por defecto** , lo usamos cuando queremos inicializar un objeto pero
no especificamos ningun valor, en nuestro caso (día, mes y anyo), si no
especificamos ningún de esos valores el compilador nos dá un error porque no
encuentra los parámetros, está esperando que le pasáramos los tres valores para
inicializarlos.

Aquí es donde entra el constructor por defecto, en nuestro ejemplo, _Fecha()_, con
ello nos aseguramos siempre inicializar de una manera genérica un objeto.

Importante saber cómo llamar a éste constructor por defecto, si lo hicieramos
tal como _Fecha h()_ estaríamos llamando a una función, debemos hacer la llamada
con _Fecha h_ tal como reflejamos en el códido ejemplo.

**El constructor de copia**, sirve para asignar los datos de un determinado constructor
y tomarlos como  propios para el de copia.

En nuestro ejemplo podríamos utilizarlo para que la _Fecha k_ tome el valor inicial
de la _Fecha f_.

Podŕiamos invocarlo como _Fecha k = f_ directamente, o con _Fecha(const Fecha&amp; F)_


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Tu Diario en Emacs <span class="tag"><span class="emacs">emacs</span></span> {#2020-03-20-Tu Diario en Emacs}

Una de las utilidades más sobresalientes que podemos emplear para intentar
ser más productivos es el uso de un Diario, con un diario vamos a poder reflejar
asiduamente nuestras tareas.

LLevando un diario de nuestras tareas conseguiremos además de ser más productivos
no desanimarnos ya que tendremos todo organizado, orden y productividad van
siempre ligados.

Emacs nos proporciona un Diario al uso muy fácil de configurar y de usar, que
junto a la herramienta calendar nos permitirá organizarnos mejor.

**Creamos el diario**
Todo se gestiona desde el fichero **_diary_**, sino lo tenemos creado en nuestro
directorio usuario, lo creamos desde nuestro terminal.

```text
$ touch ~/diary
```

**Añadimos entradas a nuestro diario**

Primero lanzamos el calendario con:

```text
M-x calendar
```

Una vez abierto podemos desplazarnos a una fecha X con:

```text
g d :'calendar-goto-date;
```

Seleccionamos la fecha deseada y usamos una de las siguientes combinaciones para
añadir entradas:

```text
i d : 'insert-diary-entry'; añade una entrada diaria
i w : 'insert-weekly-diary-entry'; añade una entrada semanal
i m : 'insert-monthly-diary-entry'; añade una entrada mensual
i y : 'insert-yearly-diary-entry'; añade una entrada anual
i a : 'insert-anniversary-diary-entry'; añade un aniversario
```

**Mostrar entradas del diario**

Dos maneras, pulsando **d** en Calendar o pulsando **s** si queremos que muestre
todas las entradas.

Ya lo tenemos!!!


## <span class="org-todo done PUBLICADA">PUBLICADA</span> C++ Referencias <span class="tag"><span class="Cpp">Cpp</span></span> {#2020-03-27-C++ Referencias}

{{< figure src="/ox-hugo/cmasmas.png" >}}

Podríamos decir que existen dos maneras de pasar argumentos a una función, el paso
por valor y el paso por referencia.
Cuando pasamos un argumento por valor estamos pasando una copia del mismo, los
cambios que hagamos en la copia no afectaran al original.

En el paso por referencia, ésta la usamos como un alias, la inicializaremos con
el nombre de otro objeto y debemos tener en cuenta que cualquier cambio en la
Referencia (alias) se hace realmente en el original.

Tienen que ser inicializadas cuando son declaradas y debemos tener en cuenta que
cualquier objeto puede ser referenciado.

**Su intaxis básica es la siguiente:**

```text
---------------------------------------------------------------------------------
Tipo del objeto ---> "&" ----> nombre de la referencia---> "=" ---> nombre target
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
int & rUnaReferencia = unEntero
---------------------------------------------------------------------------------
```

```c
#include <iostream>

int main(){
  using namespace std;
  int intUno;
  //todo lo que le ocurra a rLaRef le va a suceder a intUno
  int &rLaRef = intUno;

  intUno = 5;

  cout << "intUno "<< intUno<<endl;
  cout << "&rLaRef "<< rLaRef<<endl;
}
//Cout nos dara como salida 5 y 5
```

**&amp;** sirve tanto para saber la direccion de una variable (operador Adress-off)
como para declarar una
referencia.

**Pongamos un ejemplo:**

Ambos se refieren al objeto de la misma clase

Escritor Vargas_Llosa;
Escrito &amp;NarradorPeruano = Vargas_Llosa;

**Referencias a Objetos**

Las referencias a Objetos son usados como los objetos mismos.
Para acceder a datos y metodos se hace con el operador de acceso .
rCaballo.ObtenerPeso()

```c
#include <iostream>
class SimpleCaballo
{
public:
  //constructor
  SimpleCaballo (int edad, int peso);
  ~SimpleCaballo(){}
  int ObtenerEdad() {return suEdad;}
  int ObtenerPeso(){ return suPeso;}

private:
  int suEdad;
  int suPeso;

};

SimpleCaballo::SimpleCaballo(int edad, int peso)
{
  suEdad = edad;
  suPeso = peso;

}
int main(){
  using namespace std;
  SimpleCaballo Babieca(5,80);
  SimpleCaballo &rCaballo = Babieca;

  cout << "Babieca tiene: ";
  cout << Babieca.ObtenerPeso() << " kilos"<< endl;
}

```

**Organizar en Funciones**

**Argumentos pasados por Valor.**

```c
//Ejemplo ----Paso por Valor
#include <iostream>
using namespace std;
void intercambiar(int x, int y);

int main(){
  int x = 5;
  int y = 10;

  cout << "Main. Antes intecarmbiar, x: "<< x <<" y: "
       << y << endl;

  intercambiar(x,y);

  //Aqui no se intercambian los valores originales ya que
  //lo que se ha mandado es una copia.
  cout << " Main. Despues intercambiar, x: "<< x <<
    " y: "<< y<< endl;

  return 0;
}

void intercambiar(int x, int y)
{
  int temp;
  cout << "Intercambiar. Antes intercambiar, x: "<<x
       << " y: " << y << endl;

  temp = x;
  x = y;
  y= temp;

cout << "Intercambiar. Despues intercambiar, x: "<<x
       << " y: " << y << endl;


}

```

**Argumentos pasados por Referencia con Punteros**

```c
/Argumentos pasados por Referencia con Punteros./
//Ejemplo ----Paso por referencia usando punteros
//
#include <iostream>
using namespace std;
void intercambiar(int *x, int *y);

int main(){

  int x = 5, y = 10;
  cout << "Main, Antes intercambiar, x: "<< x << " y: "<<
    y << endl;

  intercambiar(&x,&y);

  cout << "Main, Despues intercambiar, x: " << x <<
    " y: " << endl;

  return 0;
}

void intercambiar(int *px, int *py)
{
  int tempo;
  cout << " Intercambiar. Antes intercambiar, *px: "<< *px
       << " *py: " << *py << endl;

  temp = *px;
  *px= *py;
  *py= temp;

  cout << " Intercambiar. Despues intercambiar, *px: "<< *px
       << " *py: " << *py << endl;
}
```

**Argumentos pasados por referencia con Referencias.**

```c

#include <iostream>
using namespace std;
void intercambiar(int &x, int &y);

int main(){

  int x = 5, y = 10;
  cout << "Main, Antes intercambiar, x: "<< x << " y: "<<
    y << endl;
///solamente tenemos que pasarle las variables
  intercambiar(x,y);
//cout nos imprimira los valores cambiados ya que modificamos sus referencias.
  cout << "Main, Despues intercambiar, x: " << x <<
    " y: " << endl;

  return 0;
}
//indicamos que son las referencias, todo lo que ocurre a x o y le ocurre
//a rx y ry,
void intercambiar(int &rx, int &ry)
{
  int tempo;
  cout << " Intercambiar. Antes intercambiar, *px: "<< rx
       << " *py: " << ry << endl;

  temp = rx;
  rx= ry;
  ry= temp;

  cout << " Intercambiar. Despues intercambiar, *px: "<< rx
       << " *py: " << ry << endl;

```


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Una Agenda con Org-mode <span class="tag"><span class="orgmode">orgmode</span><span class="emacs">emacs</span></span> {#2020-02-12-Agenda en Org-mode}

Una de las grandes funcionalidades que nos proporciona Emacs y Org-Mode es el
modo Agenda. Con el modo Agenda vamos a poder programar nuestro día a día de una
manera muy productiva.

La idea general es disponer de varios archivos .org de diferente temática donde
vayamos reflejando nuestras tareas pendientes.

Para ello en nuestro archivo de configuración de Emacs, configuraremos nuestra
Lista Todo Global, algo así:

```text

(setq org-agenda-files (list "/home/luis/Documentos/ORG-FILES/NotasCasa.org>
                         "/home/luis/Documentos/ORG-FILES/BLOG/NotasUni.org>
                        "/home/luis/Documentos/ORG-FILES/BLOG/tareas_globales.org>
                        "/home/luis/Documentos/ORG-FILES/BLOG/Org_Blog.org>
                       "/home/luis/Documentos/ORG-FILES/BLOG/ApuntesClase.org>
```

Podríamos asignar también un directorio completo donde tener en cuenta todos
los archivos .org alojados.

```text
(setq org-agenda-files '("~/mis-archivos-org"))
```

Como opción podríamos añadir diferentes **palabras clave** con diferentes colores
para hacer más precisa su utilización y no basarnos únicamente en las palabras
clave TODO y DONE.

Importante tener en cuenta en la configuración la posición de **"|"**, ésta nos
delimita las palabras que usamos para dar por completada una tarea.

Aquí podéis encontrar los colores tanto para el fondo como para la fuente con
sus correspondientes códigos que soporta Emacs.

<http://www.raebear.net/computers/emacs-colors/>

```text

;;CONFIGURACION ORG MODE-- PALABRAS CLAVE
(setq org-todo-keywords
'((sequence "TODO" "BORRADOR" "TRABAJANDO" "|" "DONE" "PUBLICADA" "POSPUESTO")))


;;CONFIGURACION ORG MODE-- COLORES PALABRAS CLAVE
(setq org-todo-keyword-faces
'(
("TODO"   . (:background "firebrick2" :weight bold))
("BORRADOR"  . (:foreground:"gray0" :background "orange red" :weight bold))
("DONE"   . (:foreground "gray0":background "green2" :weight bold))
("TRABAJANDO"   . (:foreground "navy" :background "azure2" :weight bold))
("PUBLICADA"   . (:foreground "gray0":background "green" :weight bold))
("POSPUESTO"  . (:foreground "gray0":background "grey70" :weight bold))
))
```

Configuraremos también las asignaciones más comunes para la Agenda,

Con **C-cl** creamos un link. Con **C-ca** llamamos al menú Agenda.

Con C-cc nos aparece org-capture, muy útil para tomar notas al vuelo y más adelante
clasificar, lo cerraríamos con **C-c C-c** volviendo automáticamente al buffer donde
estábamos.

```text
;; AGENDA
;;-------
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)

```

**Usos más frecuentes con Org-Agenda**

Con **C-c C-s** abrimos el calendario, seleccionando la fecha programaremos la tarea
donde estemos.

Abriremos Org-Agenda con **C-c a**, pulsando **a** nos muestra la agenda semanal, si
invocamos al comando con un prefijo numérico nos mostrará el número de días que
hemos seleccionado.

```text
C-u 20 C-c-a a ;;nos mostrará la agenda en los siguientes 20 días
```

-   **C-c a T** Podremos introducir una palabra clave para que sólo se nos muestren
    éstas.

-   **C-c a m** Nos mostrará las tareas con unas determinadas etiquetas.

-   **C-c a M** Igual que la anterior pero con la condición que esas tareas sean un
    TODO.

-   **C-c a t** Nos mostará todos los TODO pendientes.

-   **C-c a s** Buscaremos una cadena de texto en todos los items.

-   **C-c a n** Nos muestra tanto la planificación semanal como los TODO pendientes.

Si qureremos programar unas tareas que se repitan durante un determinado tiempo,(horas, días, meses, años) y que cuando demos por DONE una tarea no nos de por finalizada las siguientes, deberemos configurarlo de esta manera.
\#+begin_src


### T+++ [#B] Práctica libre 30' {#t-plus-plus-plus-b-práctica-libre-30}

SCHxxxxxD: &lt;2022-11-xx mié ++1d&gt;

\#+end_src


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Lector RSS Noticias en Emacs <span class="tag"><span class="emacs">emacs</span></span> {#2022-02-12-RSS News en Emacs}

Tener un lector de las noticias que se producen en los blogs que sigo es una de las experiencias más gratificante
que tengo al usar Emacs, tan sencillo como ir pasando los encabezados de las noticias y la que me interesa pulsar
intro y disfrutar del contenido. Ahí va la configuración!

cd ~/.emacs.d/plugins/

git clone <https://github.com/skeeto/elfeed.git>

En nuestro fichero de configuración añadimos esto:

```text
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PACKAGE: elfeed               ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; source: https://github.com/skeeto/elfeed.git
(add-to-list 'load-path "~/.emacs.d/plugins/elfeed/")
(require 'elfeed)

;; aqui van los feeds que nos interesen
(setq elfeed-feeds
      '(
        "http://blog.chuidiang.com/feed/"
        "http://blog.desdelinux.net/feed/"
        "http://blogubuntu.com/feed"
        "http://cucarachasracing.blogspot.com/feeds/posts/default?alt=rss"
        "http://elarmarioinformatico.blogspot.com/feeds/posts/default"
        "http://es.xkcd.com/rss/"
        "http://feeds.feedburner.com/diegocg?format=xml"
        "http://feeds.feedburner.com/Esbuntucom?format=xml"
        "http://feeds.feedburner.com/GabuntuBlog"
        "http://feeds.feedburner.com/ramonramon"
        "http://feeds.feedburner.com/teknoplof/muQI?format=xml"
        "https://xkcd.com/rss.xml"
        ))
(global-set-key [f7] 'elfeed)
(global-set-key [f8] 'elfeed-update)
```

Con b lo abrimos en nuestro navegador.
Con r marcamos la entrada como leída.


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Configurando Syncthing <span class="tag"><span class="emacs">emacs</span></span> {#2020-02-25-Y llegó Syncthing}

Syncthing apareció como un vaso de agua fresca cuando me preguntaba cómo podía usar mi agenda org en el móvil y que estuviera
actualizada constantemente con mi portatil.

Ejecutamos en cada dispositivo Syncthing

El ordenador lo consideraremos dispositivo principal, por lo tanto habrá que copiar su ID al resto, en nuestro caso, solicitaremos
desde nuestro ordenador el código QR y lo leeremos con el móvil para Añadir el dispositivo.

Seleccionando en el móvil, la opción "Introductor" en el dispositivo que hemos dado de alta (luis-pc)nos informará de todos
los dispositivos que conoce, y nos conectaremos automáticamente a ellos.

De esta forma, como nuestro dispositivo principal (luis-pc)  estará conectado a todos nuestros dispositivos, y en cada uno de estos
marcaremos al principal como Introductor, todos nuestros dispositivos estarán conectados entre sí.

En el ordenador creamos una carpeta nueva donde irán los archivos a sincronizar, autorizamos al resto de dispositivos. En los dispositivos
nos aparecerá un mensaje de permiso para compartir, y le decimos qué carpeta vamos a elegir para que sea común la sincronización.

Reiniciamos


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Agenda con Org-mode - 2 <span class="tag"><span class="emacs">emacs</span><span class="orgmode">orgmode</span></span> {#2020-02-25-Agenda con Org-mode 2}

Con éste código damos un paso más pro en nuestra Agenda.

```text
 (use-package org-agenda
      :ensure nil
      :after org
     ; :bind ([f5] . org-agenda)
      :custom
       (org-agenda-dim-blocked-tasks t)
       (org-agenda-inhibit-startup t)
       (org-agenda-show-log t)
       (org-agenda-files
       ; '( "/home/luis/Documentos/ARCHIVOS_ORG"))
         '( "/home/luis/Dropbox/ARCHIVOS_ORG"))
       (org-agenda-skip-deadline-if-done t)
       (org-agenda-skip-deadline-prewarning-if-scheduled 'pre-scheduled)
       (org-agenda-skip-scheduled-if-done t)
       (org-agenda-start-on-weekday 1)
       (org-agenda-sticky nil)
       (org-agenda-window-setup 'current-window) ;Sobrescribe la ventana actual con la agenda
       (org-agenda-tags-column -100)
       (org-agenda-time-grid '((daily today require-timed)))
       (org-agenda-use-tag-inheritance t)
       (org-enforce-todo-dependencies t)
       (org-habit-show-habits-only-for-today nil)
       (org-track-ordered-property-with-tag t)
       (org-icalendar-timezone "Europe/Madrid")
       (calendar-day-name-array ["domingo" "lunes" "martes" "miércoles"
                                  "jueves" "viernes" "sábado"])
       (calendar-month-name-array ["enero" "febrero" "marzo" "abril" "mayo"
                                    "junio" "julio" "agosto" "septiembre"
                                    "octubre" "noviembre" "diciembre"])
  (org-agenda-custom-commands
      '(("h" "Habitos" tags-todo "STYLE=\"habit\""
         ((org-agenda-overriding-header "Habitos")
          (org-agenda-sorting-strategy
           '(todo-state-down effort-up category-keep))))
              ("o" "Estudio"
          ((agenda "" )
             (tags-todo "estudio/INICIADA"
                   ((org-agenda-overriding-header "Tareas Iniciadas")
                    (org-tags-match-list-sublevels t)))
             (tags-todo "estudio/SIGUIENTE"
                        ((org-agenda-overriding-header "Siguientes Tareas")
                    (org-tags-match-list-sublevels t)))
             (tags-todo "estudio/TODO"
                   ((org-agenda-overriding-header "Tareas por Hacer")
                    (org-tags-match-list-sublevels t)))
             (tags "estudio/CANCELADA"
                   ((org-agenda-overriding-header "Tareas Canceladas")
                    (org-tags-match-list-sublevels t)))
             (tags "estudio/DONE"
                   ((org-agenda-overriding-header "Tareas terminadas sin Archivar")
                    (org-tags-match-list-sublevels t)))
             (tags "reubicar"
                   ((org-agenda-overriding-header "Reubicar")
                    (org-tags-match-list-sublevels t)))
             nil))
      ;; Reportes personales
      ("p"  "Personal"
          ((agenda "" )
             (tags-todo "personal/INICIADA"
                   ((org-agenda-overriding-header "Tareas Iniciadas")
                    (org-tags-match-list-sublevels t)))
             (tags-todo "personal/SIGUIENTE"
                   ((org-agenda-overriding-header "Siguientes Tareas")
                    (org-tags-match-list-sublevels t)))
             (tags-todo "personal/TODO"
                   ((org-agenda-overriding-header "Tareas por Hacer")
                    (org-agenda-todo-ignore-deadlines 'future)
                    (org-agenda-todo-ignore-scheduled 'future)
                    (org-agenda-tags-todo-honor-ignore-options t)
                    (org-tags-match-list-sublevels t)))
             (tags "personal/CANCELADA"
                   ((org-agenda-overriding-header "Tareas Canceladas")
                    (org-tags-match-list-sublevels t)))
             (tags "personal/PUBLICADA"
                   ((org-agenda-overriding-header "Tareas terminadas sin Archivar")
                    (org-tags-match-list-sublevels t)))
             (tags "reubicar"
                   ((org-agenda-overriding-header "Reubicar")
                    (org-tags-match-list-sublevels t)))
             nil))
)))

```


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Backup genérico <span class="tag"><span class="general">general</span></span> {#2021-01-09-Backup Sencillo}

Un backup sencillo de nuestros archivos, junto a su explicación elemental de funcionamiento.
Comprimir

```text
tar cvfz /media/usb/copia.tgz /usr/luis/Documentos
```

Descomprimir

```text
tar xvfz /media/usb/copia.tgz /usr/xxxx/Documentos
```

Script automático

```text
backup_diario.sh

-----------------------------
#!/bin/bash

_fecha=$(date +"%m_%d_%Y")
_destino="/media/usb/copia("$_fecha").tgz"

tar cfz $_destino /home/luis/Documentos

-----------------------------
#+end_src
Ejecutamos contrab -e y añadimos:
#+begin_src example
PATH=/bin
0 0 * * * /root/backup_diario.sh
:emacs
```


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Magit! controla tu Git en Emacs <span class="tag"><span class="emacs">emacs</span><span class="git">git</span></span> {#2022-10-27 Magit básico}

{{< figure src="/ox-hugo/magit.png" >}}

Hasta ahora había sido reticente en usar Magit, mi uso en el sistema Git se basa en un uso
muy básico, ya que casi todo el flujo de trabajo se lo lleva el blog.

Por naturaleza no necesito realizar un seguimiento tan exhaustivo como cuando estás en un grupo de trabajo formando parte de un proyecto.

Con el blog, simplemente me baso en hacer un stage del archivo que he generado/modificado, realizo el commit y hago push. Más básico el
uso que hago imposible, me dejo muchísimas funcionalidades que tiene ésta estupenda extensión de Emacs.

Hasta ahora me apañaba muy bien con el shell y e-shell, ya que son 3 comandos los que ejecuto, pero tengo que reconocer que el sistema
de ventanas de magit ofrece muchísima interacción e información con los cambios que haces en tu repositorio, merece mucho la pena porque
aunque de momento no saque todas las posibilidades, en un futuro si las necesito, esta herramienta hará **magia** en mi día a día.

Dejo a modo chuletilla las cuatro cosillas que hago.

**\*M - x - magit-status** , Nos pedirá ubicarnos en nuestro directorio de trabajo, y una vez situado nos mostrará cual es el estado y sus modificaciones.
Nos posicionamos en el archivo y hacemos "stage", pulsadon s.

**C-c, C-c, c**, con esta combinación realizamos un Commit, rellenamos mensaje y volvemos a pulsar C-c para salir.

**Shift p, p**  hacemos push.


## <span class="org-todo todo TODO">TODO</span> EXWM, un sistema operativo llamado EMACS <span class="tag"><span class="emacs">emacs</span></span> {#exwm-un-sistema-operativo-llamado-emacs}


## <span class="org-todo todo INICIADA">INICIADA</span> Haciendo más bonito Org-mode <span class="tag"><span class="emacs">emacs</span><span class="orgmode">orgmode</span></span> {#2022-10-27 Bullets en Org}

Hay diferentes opciones estéticas que permiten darle un aspecto más atractivo a nuestro
ecosistema org. Desde configurar con un color específico cada palabra clave a tener unos
bullets más bonitos que los simples arteriscos.

Ahí va la configuración de varias cosas.

```nil
;;COLORES PALABRAS CLAVE
(setq org-todo-keyword-faces
'(
("TODO"   . (:foreground:"gray0":background "red" :weight bold))
("INICIADA"  . (:foreground:"gray0" :background "blue" :weight bold))
("DONE"   . (:foreground "black":background "green" :weight bold))
("SIGUIENTE"   . (:foreground "gray0" :background "yellow" :weight bold))
("PUBLICADA"   . (:foreground "black":background "orange" :weight bold))
("CANCELADA"  . (:foreground "gray0":background "grey70" :weight bold))
))


;;iconos org mode
(require 'org-superstar)
(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))



```


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Org-Capture, configurando el órden <span class="tag"><span class="emacs">emacs</span><span class="orgmode">orgmode</span></span> {#2022-11-01 Org-Capture}

{{< figure src="/ox-hugo/orglogo.png" >}}

Buscando cómo mejorar mi flujo de trabajo con Emacs y su Agenda, me encontré con una de las
opciones de configuración que más pueden ayudar a tal propósito.

Org-Capture, nos presenta una combinación de teclas en la cual podemos anotar ideas, tareas, citas
de una manera muy rápida, dejándonos la opción de alojarlas en un archivo "reubicar.org", para
después alojarla en el .org que nosotros creamos más adecuado.

El flujo de trabajo sería el siguiente:

Pulsamos la combinación C-c c , y nos aparecerá un menú a seleccionar.

{{< figure src="/ox-hugo/capture1.png" >}}

Según la elección, nos aparecerá una plantilla a rellenar y tres opciones.
Si pulsamos C-c C-w reubicaremos en el árbol/fichero que queramos.
Si pulsamos C-c C-c nos almacenará la nota en un el archivo reubicar.org, para posteriormente
posicionarnos en esa nota y reubicarla a nuestro .org final con C-c C-w.
Si pulsamos C-c C-s salimos, descartando la nota.

{{< figure src="/ox-hugo/capture2.png" >}}

Nuestro código para el Init.el sería el siguiente.

```text

; Refile on top of file max
(setq org-refile-use-outline-path 'file)
; use a depth level of 6 max
(setq org-outline-path-complete-in-steps nil)
(setq org-refile-targets
      '((org-agenda-files . (:maxlevel . 4))))

(use-package org-capture
  :ensure nil
  :after org
  :bind ("C-c c" . org-capture)
  :preface
  (defvar my/org-basic-task-template "* TODO %^{Tareas}
   :PROPERTIES:
   :Effort: %^{effort|1:00|0:05|0:15|0:30|2:00|4:00}
   :END:
    Capturado %<%Y-%m-%d %H:%M>" "Plantilla básica de tareas.")

  (defvar my/org-contacts-template "* %(org-contacts-template-name)
   :PROPERTIES:
   :EMAIL: %(org-contacts-template-email)
   :PHONE: %^{123-456-789}
   :HOUSE: %^{123-456-789}
   :ALIAS: %^{nuko}
   :NICKNAME: %^{Carlos M}
   :IGNORE:
   :NOTE: %^{NOTA}
   :ADDRESS: %^{Calle Ejemplo 1 2A, 28320, Pinto, Madrid, España}
   :BIRTHDAY: %^{yyyy-mm-dd}
   :END:" "Plantilla para org-contacts.")
  :custom
  (org-capture-templates
   `(

     ("c" "Contactos" entry (file+headline "/home/luis/Dropbox/ARCHIVOS_ORG/contactos.org" "Amigos"),
      my/org-contacts-template
      :empty-lines 1)
     ("m" "Nota Música" entry (file+headline "/home/luis/Dropbox/ARCHIVOS_ORG/reubicar.org" "Nota Música"),
      my/org-basic-task-template
      :empty-lines 1)

     ("n" "Nota Ajedrez" entry (file+headline "/home/luis/Dropbox/ARCHIVOS_ORG/reubicar.org" "Ajedrez"),
      my/org-basic-task-template
      :empty-lines 1)

     ("i" "Citas" entry (file "/home/luis/Dropbox/ARCHIVOS_ORG/Diario.org" ),
      "* cita con %? \n%T"
      :empty-lines 1)

     ("b" "Blog" entry (file+headline "/home/luis/Dropbox/ARCHIVOS_ORG/reubicar.org" "Blog"),
      my/org-basic-task-template
      :empty-lines 1)

     ("t" "Tarea" entry (file+headline "/home/luis/Dropbox/ARCHIVOS_ORG/reubicar.org" "Tareas"),
      my/org-basic-task-template
      :empty-lines 1))))

```


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Un Dashboard molón en Emacs <span class="tag"><span class="emacs">emacs</span></span> {#2022-11-15 Org-Capture}

Estas cosas a mí me gustan, que la pantalla de presentación de Emacs cuando inicias te muestre esta pinta..Uff!!
Me gusta!!

{{< figure src="/ox-hugo/dashboard.png" >}}

```text

(use-package dashboard
  :preface
    (setq dashboard-banner-logo-title (concat "GNU Emacs " emacs-version
					    " kernel " (car (split-string (shell-command-to-string "uname -r") "-"))
					    " x86_64 " (car (split-string (shell-command-to-string "/usr/bin/sh -c '. /etc/os-release && echo $PRETTY_NAME'") "\n"))))
  :init
    (add-hook 'after-init-hook 'dashboard-refresh-buffer)
  :custom (dashboard-set-file-icons t)
  :custom (dashboard-startup-banner 'logo)
  :config (dashboard-setup-startup-hook))

```


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Mi archivo Config en I3WM <span class="tag"><span class="i3">i3</span></span> {#2022-12-01 Mi config i3wm}

{{< figure src="/ox-hugo/i3wmlogo.png" >}}

Llevo ya muchos años en Linux, y dos entornos de escritorio han predominado en mi vida linuxera,
uno de ellos es Gnome y el otro, el **genial i3wm**.

Éste último siempre he estado dando vueltas cómo mejorarlo, por suerte hay gente muy experta que
está dispuesta a mostrar sus habilidades en las configuraciones.

Mil gracias como siempre a gente tan genial como **Atareao, Notxor, Ugeek, y todos los compañeros de
los canales de Telegram en los grupos de Emacs y Org**.

Yo he ido recabando lo que mejor se adaptaba a mi flujo diario, y que os voy a decir, **no puedo
estar más cómodo**.

Así de bonico luce!!!

{{< figure src="/ox-hugo/miconfigi3wm.png" >}}

Dejo toda mi configuración, intentaré ir explicándola punto por punto.

```nil
# i3wm config file
# Autor:
#       Kraka
# Version:
#       v1.8: 01/11022 configuración i3wm

# Establecemos que la tecla principal para trabajar con i3 sea la de Windows
set $mod Mod4

# Establecemos que la fuente del sistema sea la System San Francisco y que el tamaño sea 10
#font pango:System San Francisco Display 10

# Al tener como mínimo 2 ventanas abiertas presionamos la tecla Windows, Posicionamos el ratón en uno de los bordes de la ventana, presionamos el botón derecho del ratón y lo arrastramos para modificar el tamaños de las ventanas
floating_modifier $mod

# El atajo de teclado para abrir una terminal será windows + Enter. Aquí también podemos seleccionar la terminal que queremos que se abra.
bindsym $mod+Return exec gnome-terminal

# La combinación de teclas para cerrar una ventana activa es windows+shift+q
bindsym $mod+Shift+q kill

# Definimos el tema y los atajos de teclado para el funcionamiento de Rofi. Si cuando Rofi está abierto presionamos Shift+cursores derecha e izquierda obtendremos funcionalidades adicionales.
#set $rofi_theme "/usr/share/rofi/themes/dmenu.rasi"
#bindsym Ctrol+d exec rofi -show run -config $rofi_theme -font "System San Francisco Display 12"
#bindsym Ctrol+Shift+space exec rofi -show window -config $rofi_theme
#bindsym $mod+d exec --no-startup-id i3-dmenu-desktop

# Rofi menu launcher
set $rofi_theme "/usr/share/rofi/themes/Adapta-Nokto.rasi"
bindsym $mod+d exec rofi -show run -config $rofi_theme
bindsym $mod+Shift+d exec rofi -show window -config $rofi_theme
# Combinación de teclas a usar para movernos entre ventanas. En mi caso uso la tecla windows+los cursores. En vez de los cursores también se pueden usar j,k,l y ñ
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+ntilde focus right
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Para mover la ventana seleccionada arriba, abajo izquierda o derecha presionando la tecla Win+Shift+cursores. En vez de los cursores también se pueden usar j,k,l y ñ
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+ntilde move right
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# Para definir que la siguiente ventana que abramos lo haga abajo o al lado de la actual que tenemos activa. Si os fijáis bien veréis un marco de color verde que indicará la posición donde se ubicará la nueva ventana
bindsym $mod+h split h
bindsym $mod+v split v

# Para poner la ventana seleccionada a pantalla completa presionar Win+f
bindsym $mod+f fullscreen toggle

# Para hacer que las ventanas se dispongan/abran en modo tiling, pestaña o apiladas. La opción predeterminada es el modo tiling
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# Para transformar una ventana de anclada a flotante y viceversa
bindsym $mod+Shift+space floating toggle

# Si la ventana activa actual está en modo tiling y queremos que la ventana activa sea una ventana flotante deberemos presionar Win+Espacio
bindsym $mod+space focus mode_toggle

# La tecla Win+a sirve para agrupar ventanas. Una vez agrupadas podremos insertar nuevas ventanas en la posición que exactamente necesitemos.
bindsym $mod+a focus parent

# Workspace naming
set $workspace1 "1: Terminal "
set $workspace2 "2: Navegador "
set $workspace3 "3: Emacs "
set $workspace4 "4: Archivos "
set $workspace5 "5: Mensajería "
set $workspace6 "6"
set $workspace7 "7"
set $workspace8 "8"
set $workspace9 "9"
set $workspace10 "10: Musica "

# switch to workspace
bindsym $mod+1 workspace $workspace1
bindsym $mod+2 workspace $workspace2
bindsym $mod+3 workspace $workspace3
bindsym $mod+4 workspace $workspace4
bindsym $mod+5 workspace $workspace5
bindsym $mod+6 workspace $workspace6
bindsym $mod+7 workspace $workspace7
bindsym $mod+8 workspace $workspace8
bindsym $mod+9 workspace $workspace9
bindsym $mod+0 workspace $workspace10



#exec -no-startup-id i3-msg 'workspace 2:Emacs; exec emacs'

bindsym $mod+Shift+m exec emacs

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $workspace1
bindsym $mod+Shift+2 move container to workspace $workspace2
bindsym $mod+Shift+3 move container to workspace $workspace3
bindsym $mod+Shift+4 move container to workspace $workspace4
bindsym $mod+Shift+5 move container to workspace $workspace5
bindsym $mod+Shift+6 move container to workspace $workspace6
bindsym $mod+Shift+7 move container to workspace $workspace7
bindsym $mod+Shift+8 move container to workspace $workspace8
bindsym $mod+Shift+9 move container to workspace $workspace9
bindsym $mod+Shift+0 move container to workspace $workspace10


#Asignando programas a espacios de trabajo
assign [class = "Gimp"] → $workspace10
assign [class = "Emacs"] → $workspace3
assign [class = "Xpdf"] → $workspace2
assign [class = "Google-chrome"] → $workspace2
assign [class = "gnome-terminal"] → $workspace1
assign [class = "Nautilus"] → $workspace4
assign [class = "Thunderbird"] → $workspace4
assign [class = "TelegramDesktop"] → $workspace5
assign [class = "Thunar"] → $workspace4

bindsym $mod+Shift+x exec xpdf
bindsym $mod+Shift+g exec gimp
bindsym $mod+Shift+n exec firefox
bindsym $mod+Shift+i exec gnome-terminal -x sh -c "irssi"
bindsym $mod+Shift+t exec gnome-terminal -x sh -c "mc"
bindsym $mod+Shift+y exec gnome-terminal -x sh -c "evolution"
bindsym $mod+Shift+u exec gnome-terminal -x sh -c "newsbeuter"
bindsym $mod+Shift+p exec libreoffice --nologo --writer
bindsym $mod+Shift+h exec eclipse

# Atajo de teclado para recargar el archivo de configuración de i3
bindsym $mod+Shift+c reload

# Atajo de teclado para reiniciar por completo el escritorio i3. No te saca de la sesión ni pierdes ningún tipo de información.
bindsym $mod+Shift+r restart

# Atajo de teclado para cerrar completamente la sesión de i3. Una vez cerrada la sesión veremos la ventana de login de Lightdm
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# Configuración para redimensionar las ventanas
mode "resize" {
        # Vim bindings
        bindsym j resize shrink width 3 px or 3 ppt
        bindsym k resize grow height 3 px or 3 ppt
        bindsym l resize shrink height 3 px or 3 ppt
        bindsym ntilde resize grow width 3 px or 3 ppt

        # Arrow bindings
        bindsym Left resize shrink width 3 px or 3 ppt
        bindsym Down resize grow height 3 px or 3 ppt
        bindsym Up resize shrink height 3 px or 3 ppt
        bindsym Right resize grow width 3 px or 3 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

# Combinación de teclas para activar el modo redimensionar ventans
bindsym $mod+r mode "resize"

# Configuración de colores mostrados en pantalla
set $bg-color	         #196609
set $border-focus	 	 #8c8c8c
set $inactive-bg-color   #56BA7C
set $text-color          #00000
set $inactive-text-color #00000
set $urgent-bg-color     #00000
set $blue-color          #5DA8F4

# Colores de las ventanas
#                       Border              Background         text                 indicator
client.focused          $border-focus       $border-focus      $text-color          #00ff00
client.unfocused        $inactive-bg-color  $inactive-bg-color $inactive-text-color #00ff00
client.focused_inactive $inactive-bg-color  $inactive-bg-color $inactive-text-color #00ff00
client.urgent           $blue-color         $blue-color        $text-color          #00ff00


# Se define parte de la configuración de la barra i3blocks
bar {
        # Bar program
#       status_command i3blocks -c ~/.i3/i3blocks.conf
        font xft: FontAwesome 12

#       status_command i3status
        status_command i3blocks -c ~/.config/i3/i3blocks.conf

        # Position of the bar
        position top

        # Bar colors
        colors {
        background $bg-color
            separator #757575
        #                  border             background         text
        focused_workspace  $bg-color          $bg-color          $text-color
        inactive_workspace $inactive-bg-color $inactive-bg-color $inactive-text-color
        urgent_workspace   $blue-color        $blue-color        $text-color
        }
}

# Quita la ventana de título y se define el grosor del borde que recubre las ventanas
for_window [class="^.*"] border pixel 1

# Mutear, incrementar y bajar el volúmen con el teclado "pactl tiene que estar instalado"
#bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +5% #increase sound volume
#bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5% #decrease sound volume
#bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle # mute sound
bindsym Mod1+Up exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +5% #increase sound volume
bindsym Mod1+Down exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -5% #decrease sound volume
bindsym Mod1+m exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle # mute sound

# Win+F9 para ir cambiando la salida de audio
#bindsym $mod+F9  exec ~/.config/i3/scripts/toggle_sink.sh

# Para incrementar o disminuir el brillo del monitor. Hace falta tener instalado xbacklight. Como no uso la opción la descomento
#bindsym XF86MonBrightnessUp exec xbacklight -inc 20 # increase screen brightness
#bindsym XF86MonBrightnessDown exec xbacklight -dec 20 # decrease screen brightness

bindsym XF86MonBrightnessUp exec xbacklight -inc 10 # increase screen brightness
bindsym XF86MonBrightnessDown exec xbacklight -dec 10 # decrease screen brightness

# Touchpad controls
#bindsym XF86TouchpadToggle exec /home/joan/bin/toggletouchpad # toggle touchpad

# Invertir el scroll del trackpad "Solo activar en equipos con trackpad"
# exec_always --no-startup-id synclient NaturalScrolling=1 VertScrollDelta=-113

# Deshabitar el trackpad mientras escribimos "Solo activar en equipos con trackpad"
# exec syndaemon -i 0.3 -t -K -R

# Asignación de las teclas multimedia
#bindsym XF86AudioPlay exec play_pause
#bindsym XF86AudioPause exec play_pause
#bindsym XF86AudioNext exec playerctl next
#bindsym XF86AudioPrev exec playerctl previous

# Atajos de teclado para lanzar programas.
#bindsym $mod+x exec i3lock --color "$bg-color" (i3lock es otra opción para bloquear la pantalla, para usarlo hay que instalar el paquete i3lock)
#bindsym $mod+Shift+g exec google-chrome
#bindsym $mod+Shift+m exec emacs
#bindsym $mod+Shift+b exec blueman-manager

# Seleccionar las aplicaciones que arrancar al iniciar i3
#exec --no-startup-id telegram-desktop
exec --no-startup-id emacs
exec --no-startup-id google-chrome
exec --no-startup-id gnome-terminal
# Para añadir el applet de Nework manager en el panel
exec --no-startup-id nm-applet
exec --no-startup-id dropbox start
#exec --no-startup-id redshift-gtk

# Hacer que se active el teclado númerico cada que que se rearranque el gestor de ventanas i3. Hay que tener instalado el paquete numlockx
exec_always --no-startup-id numlockx on

# Para establecer un fondo escritorio cada vez que se arranque o rearranque i3. "sudo apt install feh"
exec_always feh --bg-scale "/home/luis/Imágenes/Fondos de escritorio/397848.jpg"

# Hacer que la ventana de redacción de un email en thunderbird se abra en modo flotante "para ver las clases de las ventanas podemos usar xprop"
for_window [window_role="Msgcompose" class="(?i)Thunderbird"] floating enable

# Fijamos que la tecla imprimir pantalla sea encargada de realizar capturas de pantalla con la utilidad de XFCE. Una alternatica es usar flameshot
bindsym Print exec --no-startup-id xfce4-screenshooter
#bindsym Control+Print exec --no-startup-id flameshot gui

# Habilitar geoclue
# exec --no-startup-id /usr/lib/geoclue-2.0/demos/agent

# Combinación de teclas para bloquear la pantalla (Requiere el uso de Lightdm)
bindsym $mod+shift+F2 exec dm-tool lock

# Habilitar un compositor para tener transparencias y transiciones suaves. "sudo apt install compton"
exec compton -f

# Para cerrar el ordenador presionar la combinación de teclas Ctrl+Alt+S
bindsym $mod+mod1+s exec  /sbin/poweroff
#bindsym $mod+Shift+e exec i3-msg exit

bindsym --release $mod+Print exec "scrot -s -e 'mv $f ~/Dropbox/ARCHIVOS_ORG/BLOG'"

```


## <span class="org-todo done PUBLICADA">PUBLICADA</span> Calendario en Emacs <span class="tag"><span class="emacs">emacs</span></span> {#2022-11-10 Calendario en Emacs}

Una de las maravillas de organizarte con Emacs, es la **Agenda**, es incleible cómo puedes
controlar cualquier aspecto de tu flujo de trabajo diario con ella, sencilla y clara a la
hora de mostrarte tu día a día.

Con calfw, vamos a poder ver una vista de calendario completa, en la cual se reflejen tanto
nuestras citas, obligaciones como festividades o cualquier fecha relevante que creamo
oportuna.

Lo llamamos con **F5** y la vista quedaría así, reflejando todas nuestras citas y eventos progamados:

{{< figure src="/ox-hugo/calfw.png" >}}

El código para Emacs sería el siguiente:

```nil
;;---------------------------------------------------------------------------
;;----------------------CALENDARIO------------------------------------------
;;--------------------------------------------------------------------------
(use-package calfw
 :ensure t
 :bind ([f5] . mi-calendario) ;; lo llamamos con F5
 :custom
 (cfw:org-overwrite-default-keybinding m)) ;; atajos de teclado de la agenda org-mode
; (setq cfw:display-calendar-holidays nil) ;; esconder fiestas calendario emacs

 (use-package calfw-org
 :ensure t)

;; calendarios a mostrar
(defun mi-calendario ()
  (interactive)
       (cfw:open-org-calendar))
       ; :contents-sources
       ; (list
        ; (cfw:org-create-source))))

;;first day week
(setq calendar-week-start-day 1) ; 0:Domingo, 1:Lunes
(setq calendar-holidays '((holiday-fixed 1 1 "Año Nuevo")
              (holiday-fixed 1 6 "Reyes Magos")
              (holiday-fixed 4 18 "Jueves Santo")
              (holiday-fixed 4 19 "Viernes Santo")
              (holiday-fixed 5 1 "Dia del Trabajador")
              (holiday-fixed 3 5 "Cinco Marzada")
              (holiday-fixed 1 29 "San Valero")
              (holiday-fixed 3 23 "San Chorche")
              (holiday-fixed 10 12 "Día del Pilar")
              (holiday-fixed 11 01 "Todos los Santos")
              (holiday-fixed 11 09 "Almudena")
              (holiday-fixed 12 06 "Constitución")
              (holiday-fixed 12 08 "Inmaculada")
              (holiday-fixed 12 25 "Navidad")
              ))
(setq calendar-week-start-day 1) ;; la semana empieza el lunes
(setq european-calendar-style t) ;; estilo europeo

;;-------------------------------------------------------------------------------------
;;------------------------------------------------------------------------------------

```


## <span class="org-todo todo INICIADA">INICIADA</span> Backups con Clonezilla <span class="tag"><span class="linux">linux</span></span> {#2023-01-21 Backups con Clonezilla}

De las multiples maneras que siempre he tenido en cuenta para realizar imágenes de mis
ordenadores, siempre me he quedado con Clonezilla.

Clonezilla es de esas herramientas que sabes que nada va a cambiar radicalmente, siempre va a
estar ahí, una herramienta sólida y accesible para todos.

El único incoveniente, si podemos llamarlo así, es que al ser una herramienta con un uso
esporádico, siempre que voy a hacer uso de ella no recuerdo qué parámetros seguir para hacer
todo correctamente y no crear ningún desaguisado.

Este post está para eso, para cuando el Luis de dentro de unos meses no recuerde ciertas cosas
al 100%, gane en confianza y en tranquilidad al ejecutar estas importantes tareas.


### Clonación del Disco {#clonación-del-disco}

Después de inicializar Clonezilla con los parámetros normales de teclado e idioma, nos encontraremos con la pantalla:

</home/luis/Dropbox/ARCHIVOS_ORG/BLOG/clonezilla1.webp>

Elegiremos **device-device**.

</home/luis/Dropbox/ARCHIVOS_ORG/BLOG/clonezilla2.webp>

Elegimos modo experto, con ello podremos configurar diferentes parámetros haciendo el proceso más eficiente.

</home/luis/Dropbox/ARCHIVOS_ORG/BLOG/clonezilla3.webp>

Nos preguntará qué tipo de clonado queremos, elegiremos **disk to local disk**.

</home/luis/Dropbox/ARCHIVOS_ORG/BLOG/clonezilla4.webp>

Aquí elegiremos nuestro disco de origen.

</home/luis/Dropbox/ARCHIVOS_ORG/BLOG/clonezilla5.webp>

Y aquí elegiremos nuestro disco de destino

Los demás parámetros los dejamos por defecto.


### Restaurar una imagen grabada {#restaurar-una-imagen-grabada}

Iniciamos clonezilla con los parámetros normales.
Como la imagen que tenemos que recuperar se encuentra en una unidad externa, en las opciones de origen
selecionaremos **local dev**.

{{< figure src="/ox-hugo/clonezilla6.jpg" >}}

Elegimos el disco externo donde tengamos la imagen y navegamos hasta donde tengamos la imagen.

{{< figure src="/home/luis/Dropbox/ARCHIVOS_ORG/BLOG/clonezilla7.jpg" >}}

Elegimos modo beginner y seleccionamos **restoredisk**.

{{< figure src="/ox-hugo/clonezilla8.jpg" >}}

Buscamos la imagen y seleccionamos después el origen destino, donque queremos restaurar la imagen.

{{< figure src="/ox-hugo/clonezilla9.jpg" >}}

Aceptamos por defecto lo que venga y comenzará el proceso de restauración.
